//
//  AlertUtility.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/5/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

class AlertUtility{
    
    typealias MethodHandler = ()  -> Void
    
    class func showAlert(title: String, message: String, VC: UIViewController) {
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok".localized, style: .cancel, handler: { (alertAction) in
        })
        
        alertVC.addAction(ok)
        VC.present(alertVC, animated: true, completion: nil)
        
    }
    
    class func showAlertWithCompletion(title: String, message: String, VC: UIViewController, methodHandler: @escaping MethodHandler) {
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok".localized, style: .cancel, handler: { (alertAction) in
            methodHandler()
        })
        
        alertVC.addAction(ok)
        VC.present(alertVC, animated: true, completion: nil)
        
    }
    
    class func showActionSheet(titleText: String, titleFont: UIFont, titleColor: UIColor, cancelTitle: String, otherTitles: [String], VC: UIViewController, completion: @escaping (_ index : Int) -> Void)  {
        
        let titleAttributedString = NSAttributedString(string: titleText, attributes: [
            NSAttributedString.Key.font: titleFont,
            NSAttributedString.Key.foregroundColor: titleColor
        ])
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel, handler: { (action) in
            completion(0)
        })
        cancelAction.setValue(UIColor.mainColor, forKey: "titleTextColor")
        
        let alertController = UIAlertController.init(title: titleText, message: "", preferredStyle: .actionSheet)
        alertController.setValue(titleAttributedString, forKey: "attributedTitle")
        alertController.addAction(cancelAction)
        
        for string in otherTitles {
            let action = UIAlertAction.init(title: string, style: .default, handler: { (action) in
                completion(otherTitles.firstIndex(of: string)! + 1)
            })
            action.setValue(UIColor.labelColor, forKey: "titleTextColor")
            alertController.addAction(action)
        }
        
        VC.present(alertController, animated: true, completion: nil)
    }
}
