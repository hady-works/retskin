//
//  LocalizedLabel.swift
//  RetSkin
//
//  Created by Hady Hammad on 6/27/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit
import MOLH

class LocalizedLabel: UILabel{
    
    override func awakeFromNib() {
        if let txt = self.text{
            self.text = txt.localized
        }
        adjustAutoAlignment()
    }
    
    
    
    func adjustAutoAlignment(){
        if self.textAlignment != .center {
            if MOLHLanguage.isArabic() {
                self.textAlignment = .right
            }else{
                self.textAlignment = .left
            }
        }
    }
}
