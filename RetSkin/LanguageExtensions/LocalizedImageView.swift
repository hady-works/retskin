//
//  LocalizedImageView.swift
//  RetSkin
//
//  Created by Hady Hammad on 6/28/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit
import MOLH

class LocalizedImageView: UIImageView{
    override func awakeFromNib() {
        flippedDirection()
    }
    
    func flippedDirection(){
        if MOLHLanguage.isArabic() {
            self.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi));
        }
    }
}
