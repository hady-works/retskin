//
//  LocalizedButtonDirection.swift
//  RetSkin
//
//  Created by Hady Hammad on 6/27/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit
import MOLH

class LocalizedButtonDirection: UIButton{
    
    override func awakeFromNib() {
        flippedDirection()
    }
    
    func flippedDirection(){
        if MOLHLanguage.isArabic() {
            self.imageView?.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi));
        }
    }
}
