//
//  LocalizedTextField.swift
//  RetSkin
//
//  Created by Hady Hammad on 6/28/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit
import MOLH

class LocalizedTextField: UITextField{
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        adjustAutoAlignment()
    }
    
    func adjustAutoAlignment(){
        if self.textAlignment != .center {
            if MOLHLanguage.isArabic() {
                if let placeHolder = self.placeholder {
                    self.placeholder = placeHolder.localized
                }
                self.textAlignment = .right
            }else{
                
                self.textAlignment = .left
            }
        }
    }
}
