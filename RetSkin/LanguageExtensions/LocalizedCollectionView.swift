//
//  LocalizedCollectionView.swift
//  RetSkin
//
//  Created by Hady Hammad on 6/27/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit
import MOLH

class LocalizedCollectionView: UICollectionViewFlowLayout {
    override var flipsHorizontallyInOppositeLayoutDirection: Bool {
        return MOLHLanguage.isArabic()
    }
    
}
