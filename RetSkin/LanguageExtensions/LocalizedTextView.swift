//
//  LocalizedTextView.swift
//  RetSkin
//
//  Created by Hady Hammad on 6/27/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit
import MOLH

class LocalizedTextView: UITextView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        adjustAutoAlignment()
    }
    
    func adjustAutoAlignment(){
        if self.textAlignment != .center{
            if MOLHLanguage.isArabic() {
                self.textAlignment = .right
            }else{
                self.textAlignment = .left
            }
        }
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(UIResponderStandardEditActions.paste(_:)) {
            return false
        }
        return super.canPerformAction(action, withSender: sender)
    }
}
