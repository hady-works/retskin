//
//  AdminTabBar.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/6/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

class AdminTabBar: UITabBarController, UITabBarControllerDelegate {
    
    // MARK:- Instance
    static func instance () -> AdminTabBar{
        let storyboard = UIStoryboard.init(name: "Admin", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "AdminTabBar") as! AdminTabBar
    }
    
    static var article = false
    
    // MARK:- LifeCycle
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    let addBtn = UIButton(frame: CGRect.zero)
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        setupMiddleButton()
    }
    
    func setupMiddleButton() {
        addBtn.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        addBtn.layer.shadowColor = UIColor.darkGray.cgColor
        addBtn.layer.shadowOffset = CGSize(width: 0, height: 1)
        addBtn.layer.shadowRadius = 5
        addBtn.layer.shadowOpacity = 0.7
        addBtn.layer.cornerRadius = addBtn.frame.height / 2
        addBtn.addTarget(self, action: #selector(addBtnAction), for: .touchUpInside)
        var menuButtonFrame = addBtn.frame
        menuButtonFrame.origin.y = self.view.bounds.height - (addBtn.frame.height + addBtn.frame.height/2) - self.view.safeAreaInsets.bottom
        menuButtonFrame.origin.x = self.view.bounds.width/2 - menuButtonFrame.size.width/2
        addBtn.frame = menuButtonFrame
        addBtn.setTitle("+", for: .normal)
        addBtn.titleLabel?.font = UIFont(name: "Menlo-Regular", size: 35)
        addBtn.backgroundColor = UIColor.mainColor
        self.view.addSubview(addBtn)
        self.view.layoutIfNeeded()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        addBtn.frame.origin.y = self.view.bounds.height - (addBtn.frame.height + addBtn.frame.height/2) - self.view.safeAreaInsets.bottom
    }
    
    @objc func addBtnAction(sender: UIButton!) {
        if AdminTabBar.article{
            let vc = AdminAddArticleVC.instance()
            present(vc, animated: true, completion: nil)
        }else{
            let adminAddProductVC = AdminAddProductVC.instance()
            present(adminAddProductVC, animated: true, completion: nil)
        }
    }
    
}
