//
//  ArticleTVCell.swift
//  RetSkin
//
//  Created by Hady on 9/5/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit
import MOLH

class ArticleTVCell: UITableViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var articleView: UIView!
    @IBOutlet weak var articleImage: UIImageView!
    @IBOutlet weak var bodyIcon: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var contentLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    
    //MARK:- Variables
    var article: ArticleDataModel? {
        didSet{
            DispatchQueue.main.async {
                self.articleImage.setImage(imageUrl: self.article?.image ?? "")
                self.bodyIcon.setImage(imageUrl: self.article?.partLogo ?? "")
                self.titleLbl.text = MOLHLanguage.isArabic() ? self.article?.articleTitleAr : self.article?.articleTitleEn
                self.contentLbl.text = MOLHLanguage.isArabic() ? self.article?.articleContentAr : self.article?.articleContentEn
                let dateFormatter = DateFormatter()
                dateFormatter.locale = MOLHLanguage.isArabic() ? Locale(identifier: "ar_EG") : Locale(identifier: "en_US")
                dateFormatter.dateFormat = "EEEE dd/MM/yyyy"
                if let date = self.article?.createdAt {
                    let time = dateFormatter.string(from: date)
                    self.dateLbl.text = time
                }
            }
        }
    }
    
    //MARK:- LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
        articleView.shadowWithCorner(cornerRadius: 18, color: UIColor.darkGray.cgColor)
        articleImage.addCornerRadius(18)
        bodyIcon.addCornerRadius()
    }
    
}
