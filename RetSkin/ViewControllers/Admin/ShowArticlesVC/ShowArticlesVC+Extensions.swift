//
//  ShowArticlesVC+Extensions.swift
//  RetSkin
//
//  Created by Hady on 9/5/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

extension ShowArticlesVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = articlesTV.dequeue() as ArticleTVCell
        cell.article = articles[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let article = self.articles[indexPath.row]
        let vc = ArticleDetailsVC.instance()
        vc.article = article
        present(vc, animated: true, completion: nil)
    }
    
}
