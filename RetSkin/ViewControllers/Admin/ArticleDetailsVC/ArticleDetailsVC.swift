//
//  ArticleDetailsVC.swift
//  RetSkin
//
//  Created by Hady on 9/5/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit
import MOLH

class ArticleDetailsVC: BaseViewController {
    
    //MARK:- Outlet's
    @IBOutlet weak var articleImage: UIImageView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var partIcon: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var contentLbl: UILabel!
    @IBOutlet weak var removeArticleBtn: UIButton!
    
    // MARK:- Instance
    static func instance (isUser: Bool = false) -> ArticleDetailsVC{
        let vc = UIStoryboard.init(name: "Admin", bundle: nil).instantiateViewController(withIdentifier: "ArticleDetailsVC") as! ArticleDetailsVC
        vc.isUser = isUser
        return vc
    }
    
    // MARK:- Variables
    var article: ArticleDataModel?
    var isUser = false
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        bottomView.addCornerRadius(26)
        self.removeArticleBtn.isHidden = isUser
        setupUi()
    }
    
    // MARK:- Helper's
    func setupUi(){
        guard let article = self.article else { return }
        DispatchQueue.main.async {
            self.articleImage.setImage(imageUrl: article.image ?? "")
            self.partIcon.setImage(imageUrl: article.partLogo ?? "")
            self.titleLbl.text = MOLHLanguage.isArabic() ? article.articleTitleAr : article.articleTitleEn
            self.contentLbl.text = MOLHLanguage.isArabic() ? article.articleContentAr : article.articleContentEn
        }
    }
    
    //MARK:- Actions
    @IBAction func buBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buRemoveArticle(_ sender: Any) {
        guard let key = self.article?.id else{return}
        FirebaseAuthManager.shared.removeArticle(withKey: key, completionBlock: { (msg, err) in
            if err == nil {
                self.showAlertsuccess(title: msg)
                self.dismiss(animated: true, completion: nil)
            }else{
                self.showAlertError(title: msg)
            }
        })
    }
    
}
