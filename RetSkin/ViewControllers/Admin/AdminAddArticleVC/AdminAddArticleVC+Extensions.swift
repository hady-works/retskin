//
//  AdminAddArticleVC+Extensions.swift
//  RetSkin
//
//  Created by Hady on 9/5/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

// MARK:- TableView Extension
extension AdminAddArticleVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bodyPartArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = bodyPartTV.dequeue() as BodyPartTVC
        cell.configure(podyPart: bodyPartArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.2, animations: {
            let bodyPartSelected = self.bodyPartArray[indexPath.row]
            self.bodyNameSelectedLbl.text = bodyPartSelected.bodyName
            self.bodyPartSelectedId = bodyPartSelected.id ?? 0
            self.bodyPartSelectedLogo = bodyPartSelected.logo ?? ""
            self.bodyMenuView.backgroundColor = .mainColor
            self.arrowImageView.image = UIImage(named: "arrow_down")
            self.bodyNameSelectedLbl.textColor = .white
            self.bodyPartTV.isHidden = true
            self.menuShown = true
        })
    }
    
}

// MARK:- TextView Extension
extension AdminAddArticleVC: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == .lightGray {
            if textView == descEnTxtView {
                descEnTxtView.text = nil
                descEnTxtView.textColor = UIColor.black
            }else{
                descArTxtView.text = nil
                descArTxtView.textColor = UIColor.black
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            if textView == descEnTxtView {
                descEnTxtView.text = "Write Content"
                descEnTxtView.textColor = .lightGray
            }else{
                descArTxtView.text = "أكتب وصف"
                descArTxtView.textColor = .lightGray
            }
        }
    }
    
}

// MARK:- Image Picker Delegate
extension AdminAddArticleVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            articleImageView.image = image
            self.image = image
        }
        imagePicker.dismiss(animated: true, completion: nil)
    }
}

