//
//  AdminAddArticleVC.swift
//  RetSkin
//
//  Created by Hady on 9/5/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit
import Firebase

class AdminAddArticleVC: BaseViewController {
    
    // MARK:- Instance
    static func instance () -> AdminAddArticleVC{
        let storyboard = UIStoryboard.init(name: "Admin", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "AdminAddArticleVC") as! AdminAddArticleVC
    }
    
    // MARK:- Outlets
    @IBOutlet weak var articleImageView: UIImageView!
    @IBOutlet weak var nameEnView: UIView!
    @IBOutlet weak var nameEnTxtField: UITextField!
    @IBOutlet weak var nameArView: UIView!
    @IBOutlet weak var nameArTxtField: UITextField!
    @IBOutlet weak var bodyPartTV: UITableView!
    @IBOutlet weak var bodyHConstraint: NSLayoutConstraint!
    @IBOutlet weak var bodyMenuView: UIView!
    @IBOutlet weak var bodyNameSelectedLbl: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var arrowImageView: UIImageView!
    @IBOutlet weak var descEnView: UIView!
    @IBOutlet weak var descEnTxtView: UITextView!
    @IBOutlet weak var descArView: UIView!
    @IBOutlet weak var descArTxtView: UITextView!
    @IBOutlet weak var publishBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    
    //MARK:- Instance Variables
    var imagePicker:UIImagePickerController!
    var menuShown = true
    var bodyPartArray = [BodyPartDataModel]()
    var bodyPartSelectedId = 0
    var bodyPartSelectedLogo = ""
    var image: UIImage?
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
    }
    
    override func viewDidLayoutSubviews() {
        bodyPartTV.roundedFromSide(corners:[.bottomLeft, .bottomRight], cornerRadius: 15)
    }
    
    //MARK:- SetupComponents
    func setupComponents() {
        bodyPartTV.registerCellNib(cellClass: BodyPartTVC.self)
        articleImageView.addCornerRadius(10)
        bodyMenuView.addCornerRadius(15)
        mainView.addCornerRadius(15)
        nameEnView.addCornerRadius(20)
        nameArView.addCornerRadius(20)
        descEnView.addCornerRadius(20)
        descArView.addCornerRadius(20)
        publishBtn.addCornerRadius(22)
        
        descEnTxtView.textColor = .lightGray
        descEnTxtView.text = "Write Content"
        descArTxtView.textColor = .lightGray
        descArTxtView.text = "أكتب وصف"
        descEnTxtView.delegate = self
        descArTxtView.delegate = self
        
        bodyPartTV.isHidden = true
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.modalPresentationStyle = .fullScreen
        loadBodyParts()
    }
    
    func loadBodyParts() {
        FirebaseAuthManager.shared.getBodyParts(completionBlock: { (response) in
            if let bodyParts = response {
                self.bodyPartArray = bodyParts
                self.bodyHConstraint.constant = CGFloat(bodyParts.count * 55)
                self.bodyPartTV.reloadData()
            }
        })
    }
    
    // MARK:- Buttons Actions
    @IBAction func buSelectImage(_ sender: Any) {
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func buMenuPressed(_ sender: Any) {
        if menuShown{
            UIView.animate(withDuration: 0.2, animations: {
                self.bodyMenuView.backgroundColor = .backColor
                self.arrowImageView.image = UIImage(named: "arrow_up")
                self.bodyNameSelectedLbl.textColor = .darkGray
                self.bodyPartTV.isHidden = false
                self.menuShown = false
            })
        }else{
            UIView.animate(withDuration: 0.2, animations: {
                self.bodyMenuView.backgroundColor = .mainColor
                self.arrowImageView.image = UIImage(named: "arrow_down")
                self.bodyNameSelectedLbl.textColor = .white
                self.bodyPartTV.isHidden = true
                self.menuShown = true
            })
        }
    }
    
    @IBAction func buPublish(_ sender: Any) {
        
        guard let id = Auth.auth().currentUser?.uid else{return}
        
        if (nameEnTxtField.text!.isEmpty || nameArTxtField.text!.isEmpty || descEnTxtView.text!.isEmpty || descArTxtView.text!.isEmpty || descEnTxtView.text == "Write Content" || descArTxtView.text == "أكتب وصف"){
            self.showAlertWiring(title: "All Fields are required".localized)
            return
        }
        
        guard let img = image else{
            self.showAlertWiring(title: "Article image missing".localized)
            return
        }
        
        guard let enDesc = descEnTxtView.text, enDesc.count > 8 else{
            self.showAlertWiring(title: "English Description should be at least 7 characters".localized)
            return
        }
        
        guard let arDesc = descArTxtView.text, arDesc.count > 8 else{
            self.showAlertWiring(title: "Arabic Description should be at least 7 characters".localized)
            return
        }
        
        let param = ["adminId": id,
                     "articleTitleAr": nameArTxtField.text ?? "",
                     "articleTitleEn": nameEnTxtField.text ?? "",
                     "part": bodyPartSelectedId,
                     "partLogo": bodyPartSelectedLogo,
                     "articleContentAr": arDesc,
                     "articleContentEn": enDesc,
                     "creationDate": Date().timeIntervalSince1970 ] as [String : Any]
        
        self.publishArticle(param: param, withImage: img)
    }
    
    func publishArticle(param: [String:Any], withImage img: UIImage) {
        self.showLoadingIndicator()
        var values = param
        FirebaseAuthManager.shared.uploadImage(storageName: "uploads", image: img, completionBlock: { (success, url) in
            if success {
                guard let url = url else{
                    self.hideLoadingIndicator()
                    return
                }
                
                values["articleImage"] = url
                FirebaseAuthManager.shared.addArticle(value: values, completionBlock: { [weak self] (success, error) in
                    guard let self = self else { return }
                    self.hideLoadingIndicator()
                    if let error = error {
                        self.showAlertError(title: "Failed to add article".localized, body: error.localizedDescription)
                    } else {
                        self.showAlertsuccess(title: "Article Added successfully".localized)
                        self.present(AdminTabBar.instance(), animated: true, completion: nil)
                    }
                    
                })
            }
        })
    }
    
    @IBAction func buBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
