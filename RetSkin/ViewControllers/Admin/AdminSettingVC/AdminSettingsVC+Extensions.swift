//
//  AdminSettingsVC+Extensions.swift
//  RetSkin
//
//  Created by Hady Hammad on 6/28/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

extension AdminSettingsVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settingsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = adminSettingsTV.dequeue() as SettingTVCell
        cell.configure(setting: settingsArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
            
        case 0:
            let vc = AddAboutVC.instance()
            present(vc, animated: true, completion: nil)
            
        case 1:
            let vc = ChangePasswordVC.instance()
            present(vc, animated: true, completion: nil)
            
        case 2:
            let vc = AddPrivacyVC.instance()
            present(vc, animated: true, completion: nil)
            
        case 3:
            let vc = AdminHelpVC.instance()
            present(vc, animated: true, completion: nil)
            
        case 4:
            self.logOut()
            
        case 5:
            self.changeLanguage()
            
        default:
            print("error")
        }
        
    }
    
}
