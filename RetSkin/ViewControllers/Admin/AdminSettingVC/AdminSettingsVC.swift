//
//  AdminSettingsVC.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/6/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit
import MOLH

class AdminSettingsVC: BaseViewController {
    
    //MARK:- Outlet's
    @IBOutlet weak var adminSettingsTV: UITableView!
    //@IBOutlet weak var addProductBtn: UIButton!
    
    // MARK:- Instance
    static func instance () -> AdminSettingsVC{
        let storyboard = UIStoryboard.init(name: "Admin", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "AdminSettingsVC") as! AdminSettingsVC
    }
    
    var settingsArray = ["About the app", "Change password", "Privacy", "Help", "User", "Language"]
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        adminSettingsTV.registerCellNib(cellClass: SettingTVCell.self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AdminTabBar.article = false
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.animateTVList(tableView: self.adminSettingsTV)
        }
    }
    
    // MARK: - Helper's
    func changeLanguage(){
        
        AlertUtility.showActionSheet(titleText: "Choose Language".localized, titleFont: UIFont(name: "Gilroy-Medium", size: 18)!, titleColor: .mainColor, cancelTitle: "Cancel".localized, otherTitles: ["English", "العربية"], VC: self, completion: { (index) in
            
            if index == 1 {
                if MOLHLanguage.currentAppleLanguage() == "ar"{
                    MOLHLanguage.setDefaultLanguage("en")
                    MOLH.setLanguageTo("en")
                    self.setCollectionAppearance()
                    Constants.isAdmin = true
                    MOLH.reset()
                }
            }
            if index == 2 {
                if MOLHLanguage.currentAppleLanguage() == "en"{
                    MOLHLanguage.setDefaultLanguage("ar")
                    MOLH.setLanguageTo("ar")
                    self.setCollectionAppearance()
                    Constants.isAdmin = true
                    MOLH.reset()
                }
                
            }
        })
        
    }
    
    func setCollectionAppearance() {
        UICollectionView.appearance().semanticContentAttribute = MOLHLanguage.isRTLLanguage() ? .forceRightToLeft : .forceLeftToRight
    }
    
    func logOut() {
        FirebaseAuthManager.shared.logout { (success, error) in
            if success {
                let mainVC = MainTabBar.instance()
                Constants.isLogin = false
                self.present(mainVC, animated: true, completion: nil)
            }else{
                self.showAlertError(title: "Failed to logout from your account".localized, body: error?.localizedDescription ?? "")
            }
        }
    }
    
}

