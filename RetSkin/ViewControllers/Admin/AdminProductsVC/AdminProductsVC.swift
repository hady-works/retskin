//
//  AdminProductsVC.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/6/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit
import MOLH

struct Objects {
    var sectionName : String
    var sectionObjects : [ProductDataModel]
}

class AdminProductsVC: BaseViewController {
    
    // MARK:- Instance
    static func instance () -> AdminProductsVC{
        let storyboard = UIStoryboard.init(name: "Admin", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "AdminProductsVC") as! AdminProductsVC
    }
    
    // MARK:- Outlets
    //@IBOutlet weak var addProductBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet var alertView: UIView!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var removeBtn: UIButton!
    
    //MARK:- Instance Variables
    var selectedProduct: ProductDataModel?
    var objectArray = [Objects]()
    var searchObjectArray = [Objects]()
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AdminTabBar.article = false
        loadProducts()
    }
    
    //MARK:- SetupComponents
    func setupComponents() {
        tableView.registerCellNib(cellClass: AdminSearchTVCell.self)
        tableView.registerCellNib(cellClass: ProductTVC.self)
        shadowView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(shadowTapped)))
        alertView.addCornerRadius(15)
        cancelBtn.addBorderWith(width: 1, color: .mainColor)
        removeBtn.addCornerRadius(removeBtn.frame.height / 2)
        cancelBtn.addCornerRadius(cancelBtn.frame.height / 2)
    }
    
    //MARK:- Load Products
    func loadProducts() {
        self.objectArray.removeAll()
        self.objectArray.append(Objects(sectionName: "Search", sectionObjects: []))
        self.showLoadingIndicator()
        FirebaseAuthManager.shared.getSortedProducts(completionBlock: { (dict, error)  in
            self.hideLoadingIndicator()
            if let err = error {
                self.showAlertError(title: "Error".localized, body: err.localizedDescription)
            }else{
                guard let dict = dict else{return}
                for (key, value) in dict {
                    self.objectArray.append(Objects(sectionName: key, sectionObjects: value))
                }
                self.searchObjectArray = self.objectArray
                self.tableView.reloadData()
                DispatchQueue.main.async {
                    self.animateTVList(tableView: self.tableView)
                }
            }
        })
    }
    
    //MARK:- Search Methods
    func searchByText(text: String) {
        
        if text.count != 0 {
            var productsArray = [ProductDataModel]()
            self.searchObjectArray.removeAll()
            for section in self.objectArray {
                for product in section.sectionObjects{
                    let productName =  MOLHLanguage.isArabic() ? product.arName : product.enName
                    let range = productName!.lowercased().range(of: text, options: .caseInsensitive, range: nil, locale: nil)
                    if range != nil{
                        productsArray.append(product)
                    }
                }
            }
            self.searchObjectArray.append(Objects(sectionName: "Search", sectionObjects: []))
            self.searchObjectArray.append(Objects(sectionName: "Search result", sectionObjects: productsArray))
        }else{
            self.searchObjectArray = self.objectArray
        }
        
        self.tableView.reloadData()
    }
    
    func clearSearchText() {
        self.searchObjectArray.removeAll()
        for object in self.objectArray {
            self.searchObjectArray.append(object)
        }
        self.tableView.reloadData()
    }
    
    func endEditing() {
        self.searchObjectArray = self.objectArray
        self.tableView.reloadData()
    }
    
    //MARK:- Table Cells Actions
    func removeProduct(index: IndexPath){
        self.selectedProduct = objectArray[index.section].sectionObjects[index.row]
        Show_Alert_View()
    }
    
    func editProduct(product: ProductDataModel){
        let editVC = AdminAddProductVC.instance()
        editVC.editedProduct = product
        present(editVC, animated: true, completion: nil)
    }
    
    //MARK:- Actions
    @objc func shadowTapped(){
        Hide_Alert_View()
    }
    
    @IBAction func buCancelAlert(_ sender: Any) {
        Hide_Alert_View()
    }
    
    @IBAction func buRemoveProduct(_ sender: Any) {
        guard let product = selectedProduct else{return}
        FirebaseAuthManager.shared.removeProduct(product: product, completionBlock: { (msg, err) in
            if err == nil {
                self.showAlertsuccess(title: msg)
                self.loadProducts()
                self.Hide_Alert_View()
            }else{
                self.showAlertError(title: msg)
            }
        })
    }
    
    //MARK:- PopUp View
    func Hide_Alert_View() {
        self.shadowView.isHidden = true
        UIView.animate(withDuration: 0.4) {
            self.alertView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.alertView.alpha = 0
            self.alertView.removeFromSuperview()
        }
    }
    
    func Show_Alert_View() {
        self.shadowView.isHidden = false
        self.view.addSubview(alertView)
        alertView.translatesAutoresizingMaskIntoConstraints = false
        alertView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        alertView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        alertView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        alertView.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.88).isActive = true
        alertView.transform = CGAffineTransform.init(scaleX:  1.3, y: 1.3)
        alertView.alpha = 0
        UIView.animate(withDuration: 0.7) {
            self.alertView.alpha = 1
            self.alertView.transform = CGAffineTransform.identity
        }
    }
    
}
