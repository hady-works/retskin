//
//  AdminSearchTVCell.swift
//  RetSkin
//
//  Created by Hady Hammad on 7/8/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

class AdminSearchTVCell: UITableViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchText: UITextField!
    @IBOutlet weak var searchIconView: UIView!
    
    var searchAction: ((_ text: String) -> ())?
    var clearAction: (() -> ())?
    var endEditingAction: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupComponents()
    }
    
    //MARK:- SetupComponents
    func setupComponents() {
        searchView.addCornerRadius(searchView.frame.height / 2)
        searchIconView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(search)))
        searchText.delegate = self
    }
    
    @objc func search(){
        searchAction?(searchText.text!)
    }
    
}
