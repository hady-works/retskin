//
//  AdminProducts+Extensions.swift
//  RetSkin
//
//  Created by Hady Hammad on 6/26/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit
import MOLH

extension AdminProductsVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return searchObjectArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else{
            return searchObjectArray[section].sectionObjects.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0{
            let cell = tableView.dequeue() as AdminSearchTVCell
            cell.clearAction = { [weak self] in
                guard let self = self else {return}
                self.clearSearchText()
            }
            
            cell.endEditingAction = { [weak self] in
                guard let self = self else {return}
                self.endEditing()
            }
            
            cell.searchAction = { [weak self] text in
                guard let self = self else {return}
                self.searchByText(text: text)
            }
            return cell
        }else{
            
            let cell = tableView.dequeue() as ProductTVC
            cell.fromReminder = false
            cell.configure(product: searchObjectArray[indexPath.section].sectionObjects[indexPath.row])
            
            cell.removeAction = { [weak self] in
                guard let self = self else {return}
                self.removeProduct(index: indexPath)
            }
            
            cell.editingAction = { [weak self] product in
                guard let self = self else {return}
                self.editProduct(product: product)
            }
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerText = UILabel()
        headerText.textColor = #colorLiteral(red: 0.6986894011, green: 0.7201163173, blue: 0.7371560931, alpha: 1)
        headerText.font = UIFont(name: "Gilroy-Medium", size: 16)
        headerText.adjustsFontSizeToFitWidth = true
        headerText.textAlignment = .center
        
        if let key = Double(searchObjectArray[section].sectionName) {
            let date = Date(timeIntervalSince1970: key/1000)
            let formatter = DateFormatter()
            if MOLHLanguage.isArabic() {
                formatter.locale = Locale(identifier: "ar_EG")
            }
            formatter.timeZone = TimeZone.current
            formatter.dateFormat = "dd MMMM yyyy"
            headerText.text = formatter.string(from: date)
        }else{
            headerText.text = searchObjectArray[section].sectionName
        }
        return headerText
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == searchObjectArray.count - 1{
            let footerView = UIView()
            let btn = UIButton()
            btn.addCornerRadius(25)
            btn.backgroundColor = .mainColor
            btn.setTitle("See Articles".localized, for: .normal)
            btn.setTitleColor(.white, for: .normal)
            btn.titleLabel?.font = MOLHLanguage.isArabic() ? UIFont(name: "Changa-Medium", size: 18) : UIFont(name: "Gilroy-SemiBold", size: 18)
            btn.addTarget(self, action: #selector(buShowArticles(_:)), for: .touchUpInside)
            footerView.addSubview(btn)
            btn.translatesAutoresizingMaskIntoConstraints = false
            btn.heightAnchor.constraint(equalToConstant: 50).isActive = true
            btn.topAnchor.constraint(equalTo: footerView.topAnchor, constant: 32).isActive = true
            btn.trailingAnchor.constraint(equalTo: footerView.trailingAnchor, constant: -20).isActive = true
            btn.leadingAnchor.constraint(equalTo: footerView.leadingAnchor, constant: 20).isActive = true
            ///Arrow ImageView
            let imageView = UIImageView()
            imageView.image = UIImage(named: "arrow_forward")
            imageView.contentMode = .scaleAspectFit
            if MOLHLanguage.isArabic() {
                imageView.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi));
            }
            footerView.addSubview(imageView)
            imageView.translatesAutoresizingMaskIntoConstraints = false
            imageView.heightAnchor.constraint(equalToConstant: 13).isActive = true
            imageView.widthAnchor.constraint(equalToConstant: 13).isActive = true
            imageView.centerYAnchor.constraint(equalTo: btn.centerYAnchor).isActive = true
            imageView.trailingAnchor.constraint(equalTo: btn.trailingAnchor, constant: -40).isActive = true
            return footerView
        }else{
            let footerView = UIView()
            footerView.backgroundColor = .clear
            return footerView
        }
    }
    
    @objc func buShowArticles(_ sender: Any) {
        let vc = ShowArticlesVC.instance()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }else{
            return 20
        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }else if section == searchObjectArray.count - 1 {
            return 132
        }else{
            return 25
        }
    }
    
}
