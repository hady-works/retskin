//
//  MoreImagesCVC.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/11/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

class MoreImagesCVC: UICollectionViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var productView: UIView!
    @IBOutlet weak var productImageView: UIImageView!
    
    var removeAction: (() -> ())?
    
    //MARK:- LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        SetupComponents()
    }
    
    //MARK:- SetupComponents
    func SetupComponents() {
        productView.addCornerRadius(10)
        productView.addBorderWith(width: 1.5, color: .mainColor)
        productImageView.addCornerRadius(10)
    }
    
    @IBAction func removeImgBtnAction(_ sender: Any) {
        removeAction?()
    }
    
}
