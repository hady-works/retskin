//
//  AdminAddProductVC.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/6/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit
import Firebase

class AdminAddProductVC: BaseViewController {
    
    // MARK:- Instance
    static func instance () -> AdminAddProductVC{
        let storyboard = UIStoryboard.init(name: "Admin", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "AdminAddProductVC") as! AdminAddProductVC
    }
    
    // MARK:- Outlets
    @IBOutlet weak var photoView: UIView!
    @IBOutlet weak var nameEnView: UIView!
    @IBOutlet weak var nameEnTxtField: UITextField!
    @IBOutlet weak var nameArView: UIView!
    @IBOutlet weak var nameArTxtField: UITextField!
    @IBOutlet weak var bodyPartTV: UITableView!
    @IBOutlet weak var specificationEnTV: UITableView!
    @IBOutlet weak var specificationArTV: UITableView!
    @IBOutlet weak var bodyHConstraint: NSLayoutConstraint!
    @IBOutlet weak var specEnHConstraint: NSLayoutConstraint!
    @IBOutlet weak var specArHConstraint: NSLayoutConstraint!
    @IBOutlet weak var bodyMenuView: UIView!
    @IBOutlet weak var bodyNameSelectedLbl: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var arrowImageView: UIImageView!
    @IBOutlet weak var descEnView: UIView!
    @IBOutlet weak var descEnTxtView: UITextView!
    @IBOutlet weak var descArView: UIView!
    @IBOutlet weak var descArTxtView: UITextView!
    @IBOutlet weak var publishBtn: UIButton!
    @IBOutlet weak var morePhotoView: UIView!
    @IBOutlet weak var focusedImageView: UIImageView!
    @IBOutlet weak var moreImagesCV: UICollectionView!
    @IBOutlet weak var addMorePhotoBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var firstPhaseBtn: UIButton!
    @IBOutlet weak var secondPhaseBtn: UIButton!
    @IBOutlet weak var thirdPhaseBtn: UIButton!
    @IBOutlet weak var fourthPhaseBtn: UIButton!
    
    @IBOutlet weak var firstPhaseTxtfield: UITextField!
    @IBOutlet weak var secondPhaseTxtfield: UITextField!
    @IBOutlet weak var thirdPhaseTxtfield: UITextField!
    @IBOutlet weak var fourthPhaseTxtfield: UITextField!
    
    @IBOutlet weak var firstPhaseView: UIView!
    @IBOutlet weak var secondPhaseView: UIView!
    @IBOutlet weak var thirdPhaseView: UIView!
    @IBOutlet weak var fourthPhaseView: UIView!
    
    //MARK:- Instance Variables
    var imagePicker:UIImagePickerController!
    var menuShown = true
    var bodyPartArray = [BodyPartDataModel]()
    var specificationArray = [1,2,3]
    var imagesArray = [UIImage]()
    var bodyPartSelectedId = 0
    var bodyPartSelectedLogo = ""
    var urls = [String]()
    var editedProduct: ProductDataModel?
    var imagesUrlArray = [String]()
    var edit = false
    var mixedCounter = 0
    var repetitionsList = [String]()
    var imagesUrlCount = 0
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
        setupEditProduct()
    }
    
    override func viewDidLayoutSubviews() {
        bodyPartTV.roundedFromSide(corners:[.bottomLeft, .bottomRight], cornerRadius: 15)
    }
    
    //MARK:- SetupComponents
    func setupComponents() {
        self.titleLbl.text = "Add Product".localized
        moreImagesCV.registerCellNib(cellClass: MoreImagesCVC.self)
        bodyPartTV.registerCellNib(cellClass: BodyPartTVC.self)
        specificationEnTV.registerCellNib(cellClass: SpecificationTVC.self)
        specificationArTV.registerCellNib(cellClass: SpecificationTVC.self)
        
        let count = specificationArray.count
        specEnHConstraint.constant = CGFloat(count * 65)
        specArHConstraint.constant = CGFloat(count * 65)
        
        firstPhaseView.addCornerRadius()
        secondPhaseView.addCornerRadius()
        thirdPhaseView.addCornerRadius()
        fourthPhaseView.addCornerRadius()
        
        firstPhaseBtn.isSelected = true
        secondPhaseView.isHidden = true
        thirdPhaseView.isHidden = true
        fourthPhaseView.isHidden = true
        
        firstPhaseView.addBorderWith(width: 1, color: .weekBorderColor)
        secondPhaseView.addBorderWith(width: 1, color: .weekBorderColor)
        thirdPhaseView.addBorderWith(width: 1, color: .weekBorderColor)
        fourthPhaseView.addBorderWith(width: 1, color: .weekBorderColor)
        
        focusedImageView.addCornerRadius(8)
        morePhotoView.addCornerRadius(12)
        bodyMenuView.addCornerRadius(15)
        mainView.addCornerRadius(15)
        nameEnView.addCornerRadius(20)
        nameArView.addCornerRadius(20)
        descEnView.addCornerRadius(20)
        descArView.addCornerRadius(20)
        publishBtn.addCornerRadius()
        
        descEnTxtView.textColor = .lightGray
        descEnTxtView.text = "type a description"
        descArTxtView.textColor = .lightGray
        descArTxtView.text = "أضف وصف"
        descEnTxtView.delegate = self
        descArTxtView.delegate = self
        
        bodyPartTV.isHidden = true
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.modalPresentationStyle = .fullScreen
        loadBodyParts()
    }
    
    func loadBodyParts() {
        FirebaseAuthManager.shared.getBodyParts(completionBlock: { (response) in
            if let bodyParts = response {
                self.bodyPartArray = bodyParts
                self.bodyHConstraint.constant = CGFloat(bodyParts.count * 55)
                self.bodyPartTV.reloadData()
            }
        })
    }
    
    func setupEditProduct() {
        
        guard let product = editedProduct,
            let bodyPartId = product.bodyPart,
            let repetitionList = product.repetitionList,
            let spec = product.enSpecification else{return}
        
        edit = true
        titleLbl.text = "Edit".localized
        
        firstPhaseTxtfield.text  = repetitionList[0]
        secondPhaseTxtfield.text = repetitionList[1]
        thirdPhaseTxtfield.text  = repetitionList[2]
        fourthPhaseTxtfield.text = repetitionList[3]
        
        firstPhaseView.isHidden = repetitionList[0]  == ""
        secondPhaseView.isHidden = repetitionList[1] == ""
        thirdPhaseView.isHidden = repetitionList[2]  == ""
        fourthPhaseView.isHidden = repetitionList[3] == ""
        
        firstPhaseBtn.isSelected = repetitionList[0]  != ""
        secondPhaseBtn.isSelected = repetitionList[1] != ""
        thirdPhaseBtn.isSelected = repetitionList[2]  != ""
        fourthPhaseBtn.isSelected = repetitionList[3] != ""
        
        nameEnTxtField.text = product.enName
        nameArTxtField.text = product.arName
        descEnTxtView.text  = product.enDescription
        descArTxtView.text  = product.arDescription
        descEnTxtView.textColor = .black
        descArTxtView.textColor = .black
        bodyPartSelectedId   = product.bodyPart ?? 0
        bodyPartSelectedLogo = product.partLogo ?? ""
        //self.repetition = repetition
        FirebaseAuthManager.shared.getBodyPartById(id: bodyPartId, completionBlock: { part in
            self.bodyNameSelectedLbl.text = part?.bodyName
        })
        
        specificationArray.removeAll()
        for (index,_) in spec.enumerated() {
            specificationArray.append(index)
        }
        
        let count = specificationArray.count
        specEnHConstraint.constant = CGFloat(count * 65)
        specArHConstraint.constant = CGFloat(count * 65)
        
        specificationEnTV.reloadData()
        specificationArTV.reloadData()
        publishBtn.setTitle("Save".localized, for: .normal)
        
        if let images = product.images {
            self.mixedCounter = images.count
            self.imagesUrlCount = images.count
            self.imagesUrlArray = images
            morePhotoView.isHidden = false
            photoView.isHidden = true
            self.focusedImageView.setImage(imageUrl: images[images.count - 1] )
            moreImagesCV.reloadData()
            moreImagesCV.scrollToLeft(animated: true)
        }
        
    }
    
    // MARK:- Collect TableViews Data
    func getEnSpecifications() ->[String]?{
        var values = [String]()
        for (index, _) in specificationArray.enumerated() {
            let indexPath = IndexPath(row: index, section: 0)
            guard let cell = specificationEnTV.cellForRow(at: indexPath) as? SpecificationTVC else{
                return nil
            }
            if let text = cell.specTxtField.text, !text.isEmpty {
                values.append(text)
            }
        }
        return values
    }
    
    func getArSpecifications() ->[String]?{
        var values = [String]()
        for (index, _) in specificationArray.enumerated() {
            let indexPath = IndexPath(row: index, section: 0)
            guard let cell = specificationArTV.cellForRow(at: indexPath) as? SpecificationTVC else{
                return nil
            }
            if let text = cell.specTxtField.text, !text.isEmpty {
                values.append(text)
            }
        }
        return values
    }
    
    func removeImage(index: IndexPath) {
        if edit && self.imagesArray.count == 0{
            removeFromUrls(index: index)
        }else if edit && self.imagesArray.count > 0{
            if index.row > self.imagesUrlCount - 1{
                removeFromImages(index: index.row - self.imagesUrlCount)
            }else{
                removeFromUrls(index: index)
            }
        }else{
            removeFromImages(index: index.row)
        }
        
        self.moreImagesCV.reloadData()
    }
    
    func removeFromUrls(index: IndexPath) {
        self.imagesUrlArray.remove(at: index.row)
        self.mixedCounter -= 1
        self.imagesUrlCount -= 1
        if self.imagesUrlArray.count > 0{
            self.focusedImageView.setImage(imageUrl: self.imagesUrlArray[self.imagesUrlArray.count - 1] )
        }else{
            self.focusedImageView.image = nil
        }
    }
    
    func removeFromImages(index: Int) {
        self.imagesArray.remove(at: index)
        self.mixedCounter -= 1
        if self.imagesArray.count > 0{
            self.focusedImageView.image = self.imagesArray[self.imagesArray.count - 1]
        }else{
            self.focusedImageView.image = nil
        }
    }
    
    // MARK:- Buttons Actions
    @IBAction func buSelectImage(_ sender: Any) {
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func buAddMorePhotos(_ sender: Any) {
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func buMenuPressed(_ sender: Any) {
        if menuShown{
            UIView.animate(withDuration: 0.2, animations: {
                self.bodyMenuView.backgroundColor = .backColor
                self.arrowImageView.image = UIImage(named: "arrow_up")
                self.bodyNameSelectedLbl.textColor = .darkGray
                self.bodyPartTV.isHidden = false
                self.menuShown = false
            })
        }else{
            UIView.animate(withDuration: 0.2, animations: {
                self.bodyMenuView.backgroundColor = .mainColor
                self.arrowImageView.image = UIImage(named: "arrow_down")
                self.bodyNameSelectedLbl.textColor = .white
                self.bodyPartTV.isHidden = true
                self.menuShown = true
            })
        }
    }
    
    @IBAction func buAddSpecification(_ sender: Any) {
        let next = specificationArray.count + 1
        specificationArray.append(next)
        specEnHConstraint.constant = CGFloat(next * 65)
        specArHConstraint.constant = CGFloat(next * 65)
        specificationEnTV.reloadData()
        specificationArTV.reloadData()
    }
    
    @IBAction func buPublish(_ sender: Any) {
        
        if validData() {
            
            guard let enSpec = getEnSpecifications(), let arSpec = getArSpecifications() else{return}
            
            guard let id = Auth.auth().currentUser?.uid else{return}
            
            if (firstPhaseBtn.isSelected && firstPhaseTxtfield.text!.isEmpty) || (secondPhaseBtn.isSelected && secondPhaseTxtfield.text!.isEmpty) || (thirdPhaseBtn.isSelected && thirdPhaseTxtfield.text!.isEmpty) || (fourthPhaseBtn.isSelected && fourthPhaseTxtfield.text!.isEmpty){
                showAlertWiring(title: "Repetition should be from 1 to 7".localized)
                return
            }
            
            let repetitionsList = [firstPhaseTxtfield.text ?? "",
                                   secondPhaseTxtfield.text ?? "",
                                   thirdPhaseTxtfield.text ?? "",
                                   fourthPhaseTxtfield.text ?? "" ]
            
            let dictionaryValues = ["adminId": id,
                                    "productTitleAr": nameArTxtField.text ?? "",
                                    "productTitleEn": nameEnTxtField.text ?? "",
                                    "part": bodyPartSelectedId,
                                    "partLogo": bodyPartSelectedLogo,
                                    "productDescAr": descArTxtView.text ?? "",
                                    "productDescEn": descEnTxtView.text ?? "",
                                    "productSpecAr": arSpec ,
                                    "productSpecEn": enSpec ,
                                    "repetitionsList": repetitionsList ,
                                    "creationDate": Date().timeIntervalSince1970 ] as [String : Any]
            
            self.showLoadingIndicator()
            if edit{
                if imagesArray.count == 0{
                    self.updateData(value: dictionaryValues)
                }else{
                    self.updateDataWithImages(value: dictionaryValues)
                }
            }else{
                self.publishData(value: dictionaryValues)
            }
            
        }
    }
    
    func updateData(value: [String:Any]) {
        var values = value
        values["productImg"] = self.imagesUrlArray
        guard let id = self.editedProduct?.id, let date = self.editedProduct?.date else{return}
        FirebaseAuthManager.shared.updateProductData(id: id, date: date, value: values, completionBlock: { [weak self] (success, error) in
            guard let self = self else { return }
            self.hideLoadingIndicator()
            if let error = error {
                self.showAlertError(title: "Failed to update Product".localized, body: error.localizedDescription)
            } else {
                self.showAlertsuccess(title: "Edit successfully".localized)
                self.present(AdminTabBar.instance(), animated: true, completion: nil)
            }
            
        })
    }
    
    func updateDataWithImages(value: [String:Any]) {
        guard let id = self.editedProduct?.id, let date = self.editedProduct?.date else{return}
        var values = value
        //Update with image
        for image in imagesArray {
            FirebaseAuthManager.shared.uploadImage(storageName: "uploads", image: image, completionBlock: { (success, url) in
                if success {
                    guard let url = url else{
                        self.hideLoadingIndicator()
                        return
                    }
                    self.urls.append(url)
                    if self.urls.count == self.imagesArray.count {
                        values["productImg"] = self.urls + self.imagesUrlArray
                        FirebaseAuthManager.shared.updateProductData(id: id, date: date, value: values, completionBlock: { [weak self] (success, error) in
                            guard let self = self else { return }
                            self.hideLoadingIndicator()
                            if let error = error {
                                self.showAlertError(title: "Failed to update Product".localized, body: error.localizedDescription)
                            } else {
                                self.showAlertsuccess(title: "Edit successfully".localized)
                                self.present(AdminTabBar.instance(), animated: true, completion: nil)
                            }
                            
                        })
                        
                    }
                }
            })
            
        }
    }
    
    func publishData(value: [String:Any]) {
        var values = value
        for image in imagesArray {
            
            FirebaseAuthManager.shared.uploadImage(storageName: "uploads", image: image, completionBlock: { (success, url) in
                if success {
                    guard let url = url else{
                        self.hideLoadingIndicator()
                        return
                    }
                    
                    self.urls.append(url)
                    if self.urls.count == self.imagesArray.count {
                        values["productImg"] = self.urls
                        FirebaseAuthManager.shared.addProductData(value: values, completionBlock: { [weak self] (success, error) in
                            guard let self = self else { return }
                            self.hideLoadingIndicator()
                            if let error = error {
                                self.showAlertError(title: "Failed to add Product".localized, body: error.localizedDescription)
                            } else {
                                self.showAlertsuccess(title: "Added successfully".localized)
                                self.present(AdminTabBar.instance(), animated: true, completion: nil)
                            }
                            
                        })
                        
                    }
                }
            })
            
        }
    }
    
    @IBAction func buBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func firstPhaseBtnAction(_ sender: Any) {
        firstPhaseBtn.isSelected = !firstPhaseBtn.isSelected
        firstPhaseView.isHidden = !firstPhaseBtn.isSelected
    }
    
    @IBAction func secondPhaseBtnAction(_ sender: Any) {
        secondPhaseBtn.isSelected = !secondPhaseBtn.isSelected
        secondPhaseView.isHidden = !secondPhaseBtn.isSelected
    }
    
    @IBAction func thirdPhaseBtnAction(_ sender: Any) {
        thirdPhaseBtn.isSelected = !thirdPhaseBtn.isSelected
        thirdPhaseView.isHidden = !thirdPhaseBtn.isSelected
    }
    
    @IBAction func fourthPhaseBtnAction(_ sender: Any) {
        fourthPhaseBtn.isSelected = !fourthPhaseBtn.isSelected
        fourthPhaseView.isHidden = !fourthPhaseBtn.isSelected
    }
    
}
