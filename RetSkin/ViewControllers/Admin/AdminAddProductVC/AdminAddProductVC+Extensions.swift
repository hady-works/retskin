//
//  AdminAddProductVC+Extensions.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/19/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

extension AdminAddProductVC{
    
    // MARK:- Data Validations
    func validData() -> Bool{
        
        if (nameEnTxtField.text!.isEmpty || nameArTxtField.text!.isEmpty || descEnTxtView.text!.isEmpty || descArTxtView.text!.isEmpty || descEnTxtView.text == "type a description" || descArTxtView.text == "أضف وصف"){
            self.showAlertWiring(title: "All Fields are required".localized)
            return false
        }
        
        if imagesArray.count == 0 && !self.edit {
            self.showAlertWiring(title: "Product image missing".localized, body: "At least one image for product is required".localized)
            return false
        }
        
        if bodyPartSelectedId == 0 {
            self.showAlertWiring(title: "Please select the body part".localized)
            return false
        }
        
        guard let enSpec = getEnSpecifications(), enSpec.count == self.specificationArray.count else{
            self.showAlertWiring(title: "All Specifications Fields are required".localized)
            return false
        }
        
        guard let arSpec = getArSpecifications(), arSpec.count == self.specificationArray.count else{
            self.showAlertWiring(title: "All Specifications Fields are required".localized)
            return false
        }
        
        return true
    }
    
}

// MARK:- TableView Extension
extension AdminAddProductVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == bodyPartTV {
            return bodyPartArray.count
        }else{
            return specificationArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView {
            
        case bodyPartTV:
            
            let cell = bodyPartTV.dequeue() as BodyPartTVC
            cell.configure(podyPart: bodyPartArray[indexPath.row])
            return cell
            
        case specificationEnTV:
            
            let cell = specificationEnTV.dequeue() as SpecificationTVC
            cell.specTxtField.textAlignment = .left
            if let edit = self.editedProduct, edit.enSpecification!.count - 1 >= indexPath.row {
                cell.specTxtField.text = edit.enSpecification?[indexPath.row] ?? ""
            }else{
                cell.specTxtField.placeholder = "Spec \(specificationArray[indexPath.row])"
            }
            return cell
            
        case specificationArTV:
            let cell = specificationArTV.dequeue() as SpecificationTVC
            cell.specTxtField.textAlignment = .right
            
            if let edit = self.editedProduct, edit.arSpecification!.count - 1 >= indexPath.row  {
                cell.specTxtField.text = edit.arSpecification?[indexPath.row] ?? ""
            }else{
                cell.specTxtField.placeholder = "صفة \(specificationArray[indexPath.row])"
            }
            
            return cell
            
        default:
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == bodyPartTV {
            UIView.animate(withDuration: 0.2, animations: {
                let bodyPartSelected = self.bodyPartArray[indexPath.row]
                self.bodyNameSelectedLbl.text = bodyPartSelected.bodyName
                self.bodyPartSelectedId = bodyPartSelected.id ?? 0
                self.bodyPartSelectedLogo = bodyPartSelected.logo ?? ""
                self.bodyMenuView.backgroundColor = .mainColor
                self.arrowImageView.image = UIImage(named: "arrow_down")
                self.bodyNameSelectedLbl.textColor = .white
                self.bodyPartTV.isHidden = true
                self.menuShown = true
            })
        }
    }
    
}

// MARK:- CollectionView Extension
extension AdminAddProductVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if edit && mixedCounter == self.imagesUrlArray.count {
            return imagesUrlArray.count
        }else if edit && mixedCounter > 0{
            return mixedCounter
        }else{
            return imagesArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = moreImagesCV.dequeue(indexPath: indexPath) as MoreImagesCVC
        cell.removeAction = { [weak self] in
            guard let self = self else {return}
            self.removeImage(index: indexPath)
        }
        
        if edit && mixedCounter == self.imagesUrlArray.count{
            
            cell.productImageView.setImage(imageUrl: self.imagesUrlArray[indexPath.row])
            
        }else if edit && mixedCounter > 0{
            
            if indexPath.row < self.imagesUrlArray.count {
                cell.productImageView.setImage(imageUrl: self.imagesUrlArray[indexPath.row])
            }else{
                cell.productImageView.image = imagesArray[indexPath.row - self.imagesUrlArray.count]
            }
            
        }else{
            
            cell.productImageView.image = imagesArray[indexPath.row]
        }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if edit{
            if indexPath.row < self.imagesUrlArray.count {
                self.focusedImageView.setImage(imageUrl: self.imagesUrlArray[indexPath.row])
            }else{
                self.focusedImageView.image = imagesArray[indexPath.row - self.imagesUrlArray.count]
            }
        }else{
            self.focusedImageView.image = self.imagesArray[indexPath.row]
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 68, height: 68)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
}

// MARK:- TextView Extension
extension AdminAddProductVC: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == .lightGray {
            if textView == descEnTxtView {
                descEnTxtView.text = nil
                descEnTxtView.textColor = UIColor.black
            }else{
                descArTxtView.text = nil
                descArTxtView.textColor = UIColor.black
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            if textView == descEnTxtView {
                descEnTxtView.text = "type a description"
                descEnTxtView.textColor = .lightGray
            }else{
                descArTxtView.text = "أضف وصف"
                descArTxtView.textColor = .lightGray
            }
        }
    }
    
}

// MARK:- Image Picker Delegate
extension AdminAddProductVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            imagesArray.append(image)
            self.mixedCounter += 1
            focusedImageView.image = image
        }
        morePhotoView.isHidden = false
        photoView.isHidden = true
        moreImagesCV.reloadData()
        moreImagesCV.scrollToLeft(animated: true)
        imagePicker.dismiss(animated: true, completion: nil)
    }
}
