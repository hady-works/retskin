//
//  SpecificationTVC.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/8/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

class SpecificationTVC: UITableViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var specView: UIView!
    @IBOutlet weak var specTxtField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        specView.addCornerRadius(20)
    }

}
