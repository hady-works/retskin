//
//  BodyPartTVC.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/8/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit
import SDWebImage

class BodyPartTVC: UITableViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var bodyNameLbl: UILabel!
    @IBOutlet weak var bodyImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK:- Configure
    func configure(podyPart: BodyPartDataModel) {
        
        self.bodyNameLbl.text  = podyPart.bodyName
        
        if let img = podyPart.imageURL {
            DispatchQueue.main.async {
                self.bodyImageView.setImage(imageUrl: img)
            }
        }
        
    }
    
}
