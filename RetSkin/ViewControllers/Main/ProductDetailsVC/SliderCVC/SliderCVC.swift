//
//  SliderCVC.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/10/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

class SliderCVC: UICollectionViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var productImageView: UIImageView!
    
    //MARK:- LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(imageUrl: String) {
        DispatchQueue.main.async {
            self.productImageView.setImage(imageUrl: imageUrl)
        }
    }
    
}
