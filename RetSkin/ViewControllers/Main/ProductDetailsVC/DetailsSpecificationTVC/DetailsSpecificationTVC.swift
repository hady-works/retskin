//
//  DetailsSpecificationTVC.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/18/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

class DetailsSpecificationTVC: UITableViewCell {

    //MARK:- Outlets
    @IBOutlet weak var specTxtView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(specification: String){
        self.specTxtView.text = specification
    }
    
}
