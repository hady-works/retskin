//
//  ProductDetailsVC.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/9/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit
import CoreData
import MOLH

class ProductDetailsVC: BaseViewController {
    
    //MARK:- Instance
    static func instance () -> ProductDetailsVC{
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
    }
    
    //MARK:- Outlets
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var reminderView: UIView!
    @IBOutlet weak var addDeviceBtn: UIButton!
    @IBOutlet weak var reminderImageView: UIImageView!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var bodyPartIcon: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var statusImageView: UIImageView!
    
    //MARK:- Instance Variables
    var product: ProductDataModel?
    var device: Device?
    var timer = Timer()
    var counter = 0
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
        setupUI()
    }

    //MARK:- SetupComponents
    func setupComponents() {
        bottomView.shadowWithCorner(cornerRadius: 32)
        collectionView.registerCellNib(cellClass: SliderCVC.self)
        tableView.registerCellNib(cellClass: DetailsSpecificationTVC.self)
        pageControl.numberOfPages = product?.images?.count ?? 0
        reminderView.addCornerRadius(reminderView.frame.height / 2)
        reminderView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(reminderTapped)))
        addDeviceBtn.addBorderWith(width: 1, color: .mainColor)
        addDeviceBtn.addCornerRadius(addDeviceBtn.frame.height / 2)
        if Constants.getReminders().contains(self.product?.id ?? ""){
            self.statusImageView.image = UIImage(named: "done")
        }else{
            self.statusImageView.image = UIImage(named: "reminder")
        }
    }
    
    func setupUI() {
        guard let product = product else{return}
//        DispatchQueue.main.async {
//            self.bodyPartIcon.setImage(imageUrl: partLogo)
//        }
        bodyPartIcon.isHidden = true
        descriptionLbl.text = MOLHLanguage.isArabic() ?  product.arDescription : product.enDescription
        nameLbl.text = MOLHLanguage.isArabic() ?  product.arName : product.enName
        DispatchQueue.main.async {
            self.timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.changeImage), userInfo: nil, repeats: true)
        }
    }
    
    @objc func changeImage() {
        guard let imagesCount = product?.images?.count else{return}
        if counter < imagesCount {
            self.pageControl.currentPage = counter
            let index = IndexPath.init(item: counter, section: 0)
            self.collectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
            counter += 1
        } else {
            counter = 0
            self.pageControl.currentPage = counter
            let index = IndexPath.init(item: counter, section: 0)
            self.collectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: false)
            counter = 1
        }
        
    }
    
    //MARK:- Actions
    @IBAction func buBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func reminderTapped(){
        guard let productID = self.product?.id else{return}
        if Constants.reminders.contains(productID){
            self.showAlertWiring(title: "Product already in reminders".localized)
        }else{
            let addReminderVC = AddReminderVC.instance()
            addReminderVC.product = self.product
            present(addReminderVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func buAddDevice(_ sender: Any) {
        
        guard let productID = self.product?.id else{return}
        if !Constants.devices.contains(productID) {
            guard let bodyID = self.product?.bodyPart else{return}
            device = Device(context: context)
            device?.deviceEnName = self.product?.enName
            device?.deviceArName = self.product?.arName
            device?.id = self.product?.id
            device?.deviceBodyPart = Int64(bodyID)
            device?.deviceEnDesc = self.product?.enDescription
            device?.deviceArDesc = self.product?.arDescription
            device?.deviceEnSpec = self.product?.enSpecification as NSObject?
            device?.deviceArSpec = self.product?.arSpecification as NSObject?
            device?.deviceImages = self.product?.images as NSObject?
            device?.repetitions = self.product?.repetitionList as NSObject?
            device?.partLogo = self.product?.partLogo
            do{
                try context.save()
                self.showAlertsuccess(title: "Added successfully".localized)
                Constants.devices.append(productID)
            }catch let err{
                print("Error... ",err.localizedDescription)
            }
        }else{
            self.showAlertWiring(title: "Product already in devices".localized)
        }
    }
    
}
