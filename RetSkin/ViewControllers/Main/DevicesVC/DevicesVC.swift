//
//  DevicesVC.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/9/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit
import CoreData

class DevicesVC: BaseViewController {
    
    //MARK:- Instance
    static func instance () -> DevicesVC{
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "DevicesVC") as! DevicesVC
    }
    
    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet var alertView: UIView!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var removeBtn: UIButton!
    
    //MARK:- Instance Variables
    var devicesArray = [Device]()
    var selectedDevice: Device?
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadDevices()
        DispatchQueue.main.async {
            self.animateTVList(tableView: self.tableView)
        }
    }
    
    //MARK:- SetupComponents
    func setupComponents() {
        tableView.registerCellNib(cellClass: DeviceTVC.self)
        shadowView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(shadowTapped)))
        alertView.addCornerRadius(15)
        cancelBtn.addBorderWith(width: 1, color: .mainColor)
        removeBtn.addCornerRadius(removeBtn.frame.height / 2)
        cancelBtn.addCornerRadius(cancelBtn.frame.height / 2)
    }
    
    //MARK:- Load Devices
    func loadDevices() {
        let fetchedData:NSFetchRequest<Device> = Device.fetchRequest()
        do{
            devicesArray = try context.fetch(fetchedData)
            tableView.reloadData()
        }catch{
            self.showAlertError(title: "Error".localized)
        }
    }
    
    //MARK:- Actions
    @objc func shadowTapped(){
        Hide_Alert_View()
    }
    
    @IBAction func buCancelAlert(_ sender: Any) {
        Hide_Alert_View()
    }
    
    @IBAction func buRemoveDevice(_ sender: Any) {
        guard let device = self.selectedDevice else{return}
        Constants.devices.removeAll{ $0 == self.selectedDevice?.id}
        context.delete(device)
        ad.saveContext()
        self.showAlertsuccess(title: "Removed successfully".localized)
        Hide_Alert_View()
        loadDevices()
    }
    
    //MARK:- PopUp View
    func Hide_Alert_View() {
        self.shadowView.isHidden = true
        UIView.animate(withDuration: 0.4) {
            self.alertView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.alertView.alpha = 0
            self.alertView.removeFromSuperview()
        }
    }
    
    func Show_Alert_View() {
        self.shadowView.isHidden = false
        self.view.addSubview(alertView)
        alertView.translatesAutoresizingMaskIntoConstraints = false
        alertView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        alertView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        alertView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        alertView.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.88).isActive = true
        alertView.transform = CGAffineTransform.init(scaleX:  1.3, y: 1.3)
        alertView.alpha = 0
        UIView.animate(withDuration: 0.7) {
            self.alertView.alpha = 1
            self.alertView.transform = CGAffineTransform.identity
        }
    }
    
}
