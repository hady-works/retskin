//
//  DeviceTVC.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/9/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit
import MOLH

class DeviceTVC: UITableViewCell {
    //MARK:- Outlets
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var removeBtn: UIButton!
    
    //MARK:- LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupComponents()
    }
    
    //MARK:- SetupComponents
    func setupComponents() {
        mainView.shadowWithCorner(cornerRadius: 16, color: UIColor.darkGray.cgColor)
        productImage.addCornerRadius(10)
    }
    
    //MARK:- Configure
    func configure(device: Device) {
        
        self.productNameLbl.text = MOLHLanguage.isArabic() ?  device.deviceArName : device.deviceEnName
        if let images = device.deviceImages as? [String]{
            self.productImage.setImage(imageUrl: images[0])
        }
    }
    
}
