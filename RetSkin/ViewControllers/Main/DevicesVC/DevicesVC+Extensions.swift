//
//  DevicesVC+Extensions.swift
//  RetSkin
//
//  Created by Hady Hammad on 6/26/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

extension DevicesVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return devicesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue() as DeviceTVC
        cell.configure(device: self.devicesArray[indexPath.row])
        cell.removeBtn.tag = indexPath.row
        cell.removeBtn.addTarget(self, action: #selector(buRemoveItemPress(_:)), for: .touchUpInside)
        return cell
    }
    
    @objc func buRemoveItemPress(_ sender:UIButton){
        self.selectedDevice = self.devicesArray[sender.tag]
        Show_Alert_View()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let device = self.devicesArray[indexPath.row]
        let product = ProductDataModel(device: device)
        let detailsVC = ProductDetailsVC.instance()
        detailsVC.product = product
        present(detailsVC, animated: true, completion: nil)
    }
    
}
