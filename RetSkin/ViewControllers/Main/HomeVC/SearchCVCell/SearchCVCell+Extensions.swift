//
//  SearchCVCell+Extensions.swift
//  RetSkin
//
//  Created by Hady Hammad on 7/1/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit
//MARK:- TextField Extension
extension SearchCVCell: UITextFieldDelegate {
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        searchText.resignFirstResponder()
        searchText.text = ""
        clearAction?()
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchAction?(searchText.text!)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            endEditingAction?()
        }
    }
    
}
