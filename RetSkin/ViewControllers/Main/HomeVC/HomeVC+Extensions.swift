//
//  HomeVC+Extensions.swift
//  RetSkin
//
//  Created by Hady Hammad on 6/26/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

//MARK:- CollectionView Extension
extension HomeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return searchArray.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 0{
            let cell = collectionView.dequeue(indexPath: indexPath) as SearchCVCell
            cell.clearAction = { [weak self] in
                guard let self = self else {return}
                self.clearSearchText()
            }
            
            cell.endEditingAction = { [weak self] in
                guard let self = self else {return}
                self.endEditing()
            }
            
            cell.searchAction = { [weak self] text in
                guard let self = self else {return}
                self.searchByText(text: text)
            }
            
            return cell
        }else{
            let product = self.searchArray[indexPath.row - 1]
            let cell = collectionView.dequeue(indexPath: indexPath) as ProductCVC
            cell.configure(product: product)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row == 0{
            return CGSize(width: collectionView.frame.size.width - 32, height: 280)
        }else{
            return CGSize(width: collectionView.frame.size.width/2 - 20, height: 216)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row != 0{
            let detailsVC = ProductDetailsVC.instance()
            detailsVC.product = self.searchArray[indexPath.row - 1]
            present(detailsVC, animated: true, completion: nil)
        }
    }
    
}
