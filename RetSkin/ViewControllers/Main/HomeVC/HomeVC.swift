//
//  HomeVC.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/9/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit
import CoreData
import MOLH

class HomeVC: BaseViewController {
    
    //MARK:- Instance
    static func instance () -> HomeVC{
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
    }
    
    //MARK:- Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK:- Variables
    var productArray = [ProductDataModel]()
    var searchArray  = [ProductDataModel]()
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        collectionView.isHidden = true
        loadProducts()
    }
    
    //MARK:- SetupComponents
    func setupComponents() {
        collectionView.registerCellNib(cellClass: SearchCVCell.self)
        collectionView.registerCellNib(cellClass: ProductCVC.self)
    }
    
    func searchByText(text: String) {
        
        if text.count != 0 {
            
            self.searchArray.removeAll()
            for product in self.productArray  {
                let productName =  MOLHLanguage.isArabic() ? product.arName : product.enName
                let range = productName!.lowercased().range(of: text, options: .caseInsensitive, range: nil, locale: nil)
                if range != nil{
                    self.searchArray.append(product)
                }
            }
        }else{
            self.searchArray = self.productArray
        }
        
        self.collectionView.reloadData()
    }
    
    func clearSearchText() {
        self.searchArray.removeAll()
        for product in self.productArray {
            self.searchArray.append(product)
        }
        self.collectionView.reloadData()
    }
    
    func endEditing() {
        self.searchArray = self.productArray
        self.collectionView.reloadData()
    }
    
    //MARK:- Load Data
    func loadProducts() {
        self.showLoadingIndicator()
        FirebaseAuthManager.shared.getProducts(completionBlock: { (response, error)  in
            self.hideLoadingIndicator()
            if let err = error {
                self.showAlertError(title: "Error".localized, body: err.localizedDescription)
            }else{
                guard let products = response else{return}
                if products.count > 0{
                    self.collectionView.isHidden = false
                }
                self.productArray = products
                self.searchArray = products
                self.collectionView.reloadData()
                DispatchQueue.main.async {
                    self.animateCVList(collectionView: self.collectionView)
                }
            }
        })
    }
    
}
