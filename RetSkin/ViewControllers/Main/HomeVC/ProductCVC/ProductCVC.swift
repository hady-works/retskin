//
//  ProductCVC.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/9/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit
import CoreData
import MOLH

class ProductCVC: UICollectionViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var addToDevicesBtn: UIButton!
    
    //MARK:- Instance Variables
    let add = UIImage(named: "Add")
    let added = UIImage(named: "Added")
    var product: ProductDataModel?
    var device: Device?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupComponents()
    }
    
    //MARK:- SetupComponents
    func setupComponents() {
        mainView.addCornerRadius(19)
        mainView.addBorderWith(width: 0.3, color: UIColor(red: 214/255, green: 214/255, blue: 214/255, alpha: 1))
        productImage.addCornerRadius(13)
    }
    
    //MARK:- Configure
    func configure(product: ProductDataModel) {
        
        self.product = product
        guard let productId = product.id else{return}
        
        if Constants.getDevices().contains(productId){
            self.updateDevice(withID: productId)
            self.addToDevicesBtn.setImage(added, for: .normal)
        }else{
            self.addToDevicesBtn.setImage(add, for: .normal)
        }
        
        self.productNameLbl.text  = MOLHLanguage.isArabic() ?  product.arName: product.enName
        
        if let img = product.images?[0] {
            DispatchQueue.main.async {
                self.productImage.setImage(imageUrl: img)
            }
        }
        
    }
    
    //MARK:- Actions
    @IBAction func buAddToDevices(_ sender: Any) {
        guard let productId = self.product?.id else{return}
        if Constants.getDevices().contains(productId){
            removeFromDevices(with: productId)
        }else{
            addToDevices(with: productId)
        }
    }
    
    //MARK:- Manipulating core data
    func addToDevices(with id: String) {
        guard let bodyID = self.product?.bodyPart else{return}
        device = Device(context: context)
        device?.deviceEnName = self.product?.enName
        device?.deviceArName = self.product?.arName
        device?.id = self.product?.id
        device?.deviceBodyPart = Int64(bodyID)
        device?.deviceEnDesc = self.product?.enDescription
        device?.deviceArDesc = self.product?.arDescription
        device?.deviceEnSpec = self.product?.enSpecification as NSObject?
        device?.deviceArSpec = self.product?.arSpecification as NSObject?
        device?.deviceImages = self.product?.images as NSObject?
        device?.repetitions = self.product?.repetitionList as NSObject?
        device?.partLogo = self.product?.partLogo
        do{
            try context.save()
            self.addToDevicesBtn.setImage(added, for: .normal)
            Constants.devices.append(id)
        }catch let err{
            print("Error... ", err.localizedDescription)
        }
    }
    
    func removeFromDevices(with id: String) {
        let fetchedData:NSFetchRequest<Device> = Device.fetchRequest()
        do{
            let devices = try context.fetch(fetchedData)
            for device in devices {
                if device.id == id {
                    context.delete(device)
                    ad.saveContext()
                    Constants.devices.removeAll{$0 == id}
                    self.addToDevicesBtn.setImage(add, for: .normal)
                }
            }
            
        }catch let err{
            print("Error... ", err.localizedDescription)
        }
    }
    
    func updateDevice(withID id: String) {
        guard let bodyID = self.product?.bodyPart else{return}
        let fetchedData:NSFetchRequest<Device> = Device.fetchRequest()
        do{
            let devices = try context.fetch(fetchedData)
            for device in devices {
                if device.id == id {
                    device.deviceEnName = self.product?.enName
                    device.deviceArName = self.product?.arName
                    device.id = self.product?.id
                    device.deviceBodyPart = Int64(bodyID)
                    device.deviceEnDesc = self.product?.enDescription
                    device.deviceArDesc = self.product?.arDescription
                    device.deviceEnSpec = self.product?.enSpecification as NSObject?
                    device.deviceArSpec = self.product?.arSpecification as NSObject?
                    device.deviceImages = self.product?.images as NSObject?
                    device.repetitions  = self.product?.repetitionList as NSObject?
                    device.partLogo = self.product?.partLogo
                    ad.saveContext()
                }
            }
            
        }catch let err{
            print("Error... ", err.localizedDescription)
        }
    }
    
}
