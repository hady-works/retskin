//
//  AddReminderVC.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/9/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit
import FSCalendar
import EventKit
import CoreData
import MOLH

class AddReminderVC: BaseViewController {
    
    //MARK:- Instance
    static func instance () -> AddReminderVC{
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "AddReminderVC") as! AddReminderVC
    }
    
    //MARK:- Outlets
    @IBOutlet weak var productView: UIView!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var phasesTV: UITableView!
    
    //MARK:- Instance Variables
    var product: ProductDataModel?
    var startDate: Date?
    var alarm: EKAlarm?
    var reminder: Reminder?
    var time = Date()
    var eventIDs = [String]()
    let dayDouble = 24 * 60 * 60
    var edit = false
    var deletedReminder: Reminder?
    var selectedIndex: Int?
    var repetition: Int?
    var repetitionString = ""
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
        mappingDeletedReminder()
        showProductData()
    }
    
    //MARK:- SetupComponents
    func setupComponents() {
        
        calendar.delegate = self
        calendar.dataSource = self
        phasesTV.registerCellNib(cellClass: PhasesTVCell.self)
        productView.shadowWithCorner(cornerRadius: 15, color: UIColor.darkGray.cgColor)
        timeView.shadowWithCorner(cornerRadius: 15, color: UIColor.darkGray.cgColor)
        confirmBtn.addCornerRadius(22)
        datePicker.setValue(UIColor.mainColor, forKeyPath: "textColor")
        
        calendar.appearance.headerTitleFont = UIFont(name: "Gilroy-SemiBold", size: 17)!
        calendar.appearance.weekdayFont = UIFont(name: "Gilroy-SemiBold", size: 16)!
        calendar.appearance.titleFont = UIFont(name: "Gilroy-SemiBold", size: 15)!
        
        self.productImageView.addCornerRadius(6)
        self.startDate = calendar.today
        titleLbl.text = edit ? "Edit".localized : "Add Reminder".localized
    }
    
    func authorize() {
        
        let eventStore = EKEventStore()
        switch EKEventStore.authorizationStatus(for: .event) {
            
        case .authorized:
            DispatchQueue.main.async {
                self.insertEvent(store: eventStore) { error in
                    if let err = error{
                        DispatchQueue.main.async {
                            self.showAlertError(title: "Failed to save event".localized, body: "\(err.localizedDescription)")
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.saveReminderProduct()
                        }
                    }
                }
            }
            
        case .denied:
            print("Access denied")
            
        case .notDetermined:
            
            eventStore.requestAccess(to: .event, completion:
                {[weak self] (granted: Bool, error: Error?) -> Void in
                    if granted {
                        DispatchQueue.main.async {
                            self!.insertEvent(store: eventStore) { error in
                                if let err = error{
                                    DispatchQueue.main.async {
                                        self?.showAlertError(title: "Failed to save event".localized, body: "\(err.localizedDescription)")
                                    }
                                }else{
                                    DispatchQueue.main.async {
                                        self?.saveReminderProduct()
                                    }
                                }
                            }
                        }
                    } else {
                        print("Access denied")
                    }
            })
            
        default:
            print("Case default")
        }
        
    }
    
    //MARK:- Load Devices
    func mappingDeletedReminder() {
        let fetchedData:NSFetchRequest<Reminder> = Reminder.fetchRequest()
        do{
            let result = try context.fetch(fetchedData)
            for reminder in result{
                if reminder.id == product?.id{
                    self.deletedReminder = reminder
                }
            }
            
        }catch{
            DispatchQueue.main.async {
                self.showAlertError(title: "Error".localized)
            }
        }
    }
    
    //MARK:- Show Product data
    func showProductData() {
        guard let product = self.product else{return}
        self.productNameLbl.text = MOLHLanguage.isArabic() ? product.arName : product.enName
        DispatchQueue.main.async {
            if let img = product.images?[0] {
                self.productImageView.setImage(imageUrl: img)
                if let time = self.deletedReminder?.time {
                    self.startDate = self.deletedReminder?.startDate
                    self.calendar.select(time, scrollToDate: true)
                    self.datePicker.setDate(time, animated: true)
                }
            }
        }
    }
    
    // MARK:- Actions
    @IBAction func buBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buConfirmReminder(_ sender: Any) {
        DispatchQueue.main.async {
            self.authorize()
        }
    }
    
    func insertEvent(store: EKEventStore, completion: @escaping (_ error: Error?) -> ()) {
        
        guard let rep = self.repetition else{
            showAlertWiring(title: "Please select repetition".localized)
            return
        }
        
        for start in getStartDates(rep: rep) {
            
            let event = EKEvent(eventStore: store)
            
            event.title = "RetSkin"
            event.startDate = start
            event.endDate = start.addingTimeInterval(10 * 60)
            event.notes = MOLHLanguage.isArabic() ? self.product?.arName : self.product?.enName
            event.calendar = store.defaultCalendarForNewEvents
            
            self.alarm = EKAlarm(absoluteDate: start)
            if let alarm = self.alarm {
                event.addAlarm(alarm)
            }
            
            do {
                try store.save(event, span: .thisEvent, commit: true)
                self.eventIDs.append(event.eventIdentifier)
            } catch let error as NSError {
                completion(error)
            }
        }
        
        completion(nil)
    }
    
    func getStartDates(rep: Int) -> [Date] {
        
        var dates = [Date]()
        var start = Date()
        
        if let startDate = self.startDate {
            
            var dateComponents = DateComponents()
            dateComponents.hour = Calendar.current.component(.hour, from: self.datePicker.date)
            dateComponents.minute = Calendar.current.component(.minute, from: self.datePicker.date)
            
            if let date = Calendar.current.date(byAdding: dateComponents, to: startDate) {
                start = date
                self.time = date
            }
            
            dates.append(start)
            for (i,_) in (product?.repetitionList?.enumerated())!{
                let phase = Double(i + 1)
                let numOfRepeatFo2Months = Int(ceil(8 / phase) * Double(rep))
                let dayFactorToAdd = 60 / numOfRepeatFo2Months
                for day in 1...numOfRepeatFo2Months {
                    dates.append(startDate + (TimeInterval(day * dayFactorToAdd * dayDouble)))
                }
            }
        }
        return dates
    }
    
    func saveReminderProduct() {
        
        guard let bodyID = self.product?.bodyPart,
            let id = self.product?.id else{return}
        
        reminder = Reminder(context: context)
        reminder?.deviceEnName = self.product?.enName
        reminder?.deviceArName = self.product?.arName
        reminder?.id = self.product?.id
        reminder?.deviceBodyPart = Int64(bodyID)
        reminder?.deviceEnDesc = self.product?.enDescription
        reminder?.deviceArDesc = self.product?.arDescription
        reminder?.deviceEnSpec = self.product?.enSpecification as NSObject?
        reminder?.deviceArSpec = self.product?.arSpecification as NSObject?
        reminder?.deviceImages = self.product?.images as NSObject?
        reminder?.repetitionsList = self.product?.repetitionList as NSObject?
        reminder?.repetition = self.repetitionString
        reminder?.time = self.time
        reminder?.startDate = self.startDate
        reminder?.eventsIdentifiers = self.eventIDs as NSObject?
        reminder?.partLogo = self.product?.partLogo
        
        do{
            try context.save()
            Constants.reminders.append(id)
            DispatchQueue.main.async {
                if self.edit{
                    self.editReminder()
                }else{
                    self.completeSuccess(str: "Added")
                }
            }
        }catch let err{
            DispatchQueue.main.async {
                self.showAlertError(title: "Failed to save event".localized, body: "\(err.localizedDescription)")
            }
        }
    }
    
    //Remove event and then remove reminder from CoreData
    func editReminder() {
        self.removeEvent() { error in
            if let err = error{
                DispatchQueue.main.async {
                    self.showAlertError(title: "Failed to save event".localized, body: "\(err.localizedDescription)")
                }
            }else{
                guard let reminder = self.deletedReminder else{return}
                self.deleteReminder(reminder: reminder)
            }
        }
    }
    
    func deleteReminder(reminder: Reminder) {
        guard let reminderID = reminder.id else{return}
        DispatchQueue.main.async {
            context.delete(reminder)
            ad.saveContext()
            Constants.reminders.removeAll{ $0 == reminderID}
            self.completeSuccess(str: "Edit")
        }
    }
    
    func completeSuccess(str: String) {
        self.showAlertsuccess(title: "\(str) successfully".localized)
        let mainVC = MainTabBar.instance()
        mainVC.index = 1
        self.present(mainVC, animated: true, completion: nil)
    }
    
    func removeEvent(completion: @escaping (_ error: Error?) -> ()) {
        
        guard let identifiers = self.deletedReminder?.eventsIdentifiers as? [String] else{return}
        let eventStore : EKEventStore = EKEventStore()
        for identifier in identifiers {
            if let event = eventStore.event(withIdentifier: identifier){
                do {
                    try eventStore.remove(event, span: .thisEvent, commit: true)
                } catch let error as NSError {
                    completion(error)
                }
            }
            
        }
        completion(nil)
    }
    
}
