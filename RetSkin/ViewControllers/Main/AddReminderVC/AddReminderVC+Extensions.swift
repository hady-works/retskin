//
//  AddReminderVC+Extensions.swift
//  RetSkin
//
//  Created by Hady Hammad on 6/26/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit
import FSCalendar

extension AddReminderVC: FSCalendarDelegate, FSCalendarDataSource {
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        self.startDate = date
    }
}

extension AddReminderVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return product?.repetitionList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = phasesTV.dequeue() as PhasesTVCell
        cell.isSelected = indexPath.row == selectedIndex ? true : false
        cell.repetition = product?.repetitionList?[indexPath.row] ?? ""
        cell.index = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let rep = product?.repetitionList?[indexPath.row] ?? ""
        repetition = Int(rep)
        selectedIndex = indexPath.row
        
        switch indexPath.row {
        case 0:
            repetitionString = rep + " Reminders every week".localized
            
        case 1:
            repetitionString = rep + " Reminders every two weeks".localized
            
        case 2:
            repetitionString = rep + " Reminders every three weeks".localized
            
        case 3:
            repetitionString = rep + " Reminders every four weeks".localized
            
        default:
            print("error")
        }
        
        phasesTV.reloadData()
    }
    
}
