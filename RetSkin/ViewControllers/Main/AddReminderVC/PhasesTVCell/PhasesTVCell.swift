//
//  PhasesTVCell.swift
//  RetSkin
//
//  Created by Hady on 10/11/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

class PhasesTVCell: UITableViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var phaseLbl: UILabel!
    @IBOutlet weak var weeksLbl: UILabel!
    @IBOutlet weak var phaseView: UIView!
    
    var repetition = ""
    
    var index: Int = 0{
        didSet{
            switch index {
            case 0:
                phaseLbl.text = "First phase".localized
                weeksLbl.text = repetition == "" ? "Not Available".localized : repetition + " Reminders every week".localized
                
            case 1:
                phaseLbl.text = "Second phase".localized
                weeksLbl.text = repetition == "" ? "Not Available".localized : repetition + " Reminders every two weeks".localized
                
            case 2:
                phaseLbl.text = "Third phase".localized
                weeksLbl.text = repetition == "" ? "Not Available".localized :  repetition + " Reminders every three weeks".localized
                
            case 3:
                phaseLbl.text = "Fourth phase".localized
                weeksLbl.text = repetition == "" ? "Not Available".localized : repetition + " Reminders every four weeks".localized
                
            default:
                print("error")
            }
        }
    }
    
    //MARK:- LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    //MARK:- SetupUI
    func setupUI() {
        phaseView.addCornerRadius()
        phaseView.backgroundColor = .white
        phaseView.addBorderWith(width: 1.5, color: .weekBorderColor)
    }
    
    override var isSelected: Bool {
        didSet{
            if repetition == ""{
                phaseView.addBorderWith(width: 1.5, color: .backColor)
                phaseView.backgroundColor = .backColor
            }else{
                phaseView.backgroundColor = isSelected ? .mainColor : .white
                weeksLbl.textColor = isSelected ? .white : .weekBorderColor
            }
        }
    }
    
}
