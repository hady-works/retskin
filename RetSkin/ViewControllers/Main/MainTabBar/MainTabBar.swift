//
//  MainTabBar.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/10/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

class MainTabBar: UITabBarController {
    
    // MARK:- Instance
    static func instance () -> MainTabBar{
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "MainTabBar") as! MainTabBar
    }
    
    var index: Int = 0{
        didSet{
            self.selectedIndex = index
        }
    }
    
    public let coustmeTabBarView:UIView = {
        // daclare coustmeTabBarView as view
        let view = UIView(frame: .zero)
        // to make the cornerRadius of coustmeTabBarView
        view.backgroundColor = .white
        view.layer.cornerRadius = 20
        view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        view.clipsToBounds = true
        // to make the shadow of coustmeTabBarView
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: -3.0)
        view.layer.shadowOpacity = 0.15
        view.layer.shadowRadius = 10.0
        return view
    }()
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        addcoustmeTabBarView()
        hideTabBarBorder()
    }
    
    func hideTab (){
        self.coustmeTabBarView.alpha = 0
        self.tabBar.isHidden = true
    }
    
    func showTab (){
        self.coustmeTabBarView.alpha = 1
        self.tabBar.isHidden = false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        coustmeTabBarView.frame = tabBar.frame
    }
    
    private func addcoustmeTabBarView() {
        coustmeTabBarView.frame = tabBar.frame
        view.addSubview(coustmeTabBarView)
        view.bringSubviewToFront(self.tabBar)
    }
    
    func hideTabBarBorder() {
        let tabBar = self.tabBar
        tabBar.backgroundImage = UIImage.from(color: .clear)
        tabBar.shadowImage = UIImage()
        tabBar.clipsToBounds = true
    }
}
