//
//  ReminderVC.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/9/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit
import CoreData
import EventKit

class ReminderVC: BaseViewController {
    
    //MARK:- Instance
    static func instance () -> ReminderVC{
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ReminderVC") as! ReminderVC
    }
    
    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet var alertView: UIView!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var removeBtn: UIButton!
    
    //MARK:- Instance Variables
    var remindersArray = [ProductDataModel]()
    var reminderDelArray = [Reminder]()
    var selectedProduct: Reminder?
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadReminderes()
        DispatchQueue.main.async {
            self.animateTVList(tableView: self.tableView)
        }
    }
    
    //MARK:- SetupComponents
    func setupComponents() {
        tableView.registerCellNib(cellClass: ProductTVC.self)
        shadowView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(shadowTapped)))
        alertView.addCornerRadius(15)
        cancelBtn.addBorderWith(width: 1, color: .mainColor)
        removeBtn.addCornerRadius(removeBtn.frame.height / 2)
        cancelBtn.addCornerRadius(cancelBtn.frame.height / 2)
    }
    
    //MARK:- Load Devices
    func loadReminderes() {
        self.remindersArray.removeAll()
        let fetchedData:NSFetchRequest<Reminder> = Reminder.fetchRequest()
        do{
            reminderDelArray = try context.fetch(fetchedData)
            for reminder in reminderDelArray{
                let product = ProductDataModel(reminder: reminder)
                self.remindersArray.append(product)
            }
            tableView.reloadData()
        }catch{
            self.showAlertError(title: "Error in getting reminders".localized)
        }
    }
    
    //MARK:- Actions
    @objc func shadowTapped(){
        Hide_Alert_View()
    }
    
    @IBAction func buCancelAlert(_ sender: Any) {
        Hide_Alert_View()
    }
    
    @IBAction func buRemoveReminder(_ sender: Any) {
        
        removeReminder(){ error in
            if let err = error{
                self.showAlertError(title: "Failed to remove event", body: "\(err.localizedDescription)")
            }else{
                guard let reminder = self.selectedProduct, let reminderID = reminder.id else{return}
                DispatchQueue.main.async {
                    context.delete(reminder)
                    ad.saveContext()
                    Constants.reminders.removeAll{ $0 == reminderID}
                    self.showAlertsuccess(title: "Removed successfully".localized)
                    self.Hide_Alert_View()
                    self.loadReminderes()
                }
            }
        }
        
    }
    
    func removeReminder(completion: (_ error: Error?) -> ()) {
        
        guard let identifiers = self.selectedProduct?.eventsIdentifiers as? [String] else{return}
        for ident in identifiers {
            
            let eventStore : EKEventStore = EKEventStore()
            if let event = eventStore.event(withIdentifier: ident){
                
                do {
                    try eventStore.remove(event, span: .thisEvent, commit: true)
                } catch let err as NSError {
                    completion(err)
                }
            }
        }
        completion(nil)
    }
    
    //MARK:- Table Cells Actions
    func removeProduct(index: IndexPath){
        self.selectedProduct = self.reminderDelArray[index.row]
        Show_Alert_View()
    }
    
    func editProduct(product: ProductDataModel){
        let editVC = AddReminderVC.instance()
        editVC.product = product
        editVC.edit = true
        present(editVC, animated: true, completion: nil)
    }
    
    //MARK:- PopUp View
    func Hide_Alert_View() {
        self.shadowView.isHidden = true
        UIView.animate(withDuration: 0.4) {
            self.alertView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.alertView.alpha = 0
            self.alertView.removeFromSuperview()
        }
    }
    
    func Show_Alert_View() {
        self.shadowView.isHidden = false
        self.view.addSubview(alertView)
        alertView.translatesAutoresizingMaskIntoConstraints = false
        alertView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        alertView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        alertView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        alertView.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.88).isActive = true
        alertView.transform = CGAffineTransform.init(scaleX:  1.3, y: 1.3)
        alertView.alpha = 0
        UIView.animate(withDuration: 0.7) {
            self.alertView.alpha = 1
            self.alertView.transform = CGAffineTransform.identity
        }
    }
    
}
