//
//  RemindersVC+Extensions.swift
//  RetSkin
//
//  Created by Hady Hammad on 6/26/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

extension ReminderVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return remindersArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue() as ProductTVC
        cell.fromReminder = true
        cell.configure(product: self.remindersArray[indexPath.row])
        cell.removeAction = { [weak self] in
            guard let self = self else {return}
            self.removeProduct(index: indexPath)
        }
        
        cell.editingAction = { [weak self] product in
            guard let self = self else {return}
            self.editProduct(product: product)
        }
        return cell
    }
    
}
