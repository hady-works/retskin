//
//  ProductTVC.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/9/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit
import MOLH

class ProductTVC: UITableViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var daysLbl: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var removeBtn: UIButton!
    
    //MARK:- Properties
    var product: ProductDataModel?
    var removeAction: (() -> ())?
    var editingAction: ((_ product: ProductDataModel) -> ())?
    var fromReminder = true
    
    //MARK:- LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupComponents()
    }
    
    //MARK:- SetupComponents
    func setupComponents() {
        mainView.shadowWithCorner(cornerRadius: 16, color: UIColor.darkGray.cgColor)
        productImage.addCornerRadius(10)
    }
    
    //MARK:- Configure
    func configure(product: ProductDataModel) {
        self.product = product
        
        if let date = product.createdAt {
            self.daysLbl.text = fromReminder ? self.product?.repetition : date.getString(format: "HH:mm a")
        }
        
        self.productNameLbl.text = MOLHLanguage.isArabic() ?  product.arName: product.enName
        DispatchQueue.main.async {
            if let img = product.images?[0] {
                self.productImage.setImage(imageUrl: img)
            }
        }
    }
    
    //MARK:- Action's
    @IBAction func removeActionTapped(_ sender: Any) {
        removeAction?()
    }
    
    @IBAction func editActionTapped(_ sender: Any) {
        if let product = self.product {
            editingAction?(product)
        }
    }
    
}

