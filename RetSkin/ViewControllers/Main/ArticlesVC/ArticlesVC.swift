//
//  ArticlesVC.swift
//  RetSkin
//
//  Created by Hady on 9/6/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

class ArticlesVC: BaseViewController {
    
    //MARK:- Outlet's
    @IBOutlet weak var articlesTV: UITableView!
    
    // MARK:- Instance
    static func instance () -> ArticlesVC{
        let storyboard = UIStoryboard.init(name: "Admin", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ArticlesVC") as! ArticlesVC
    }
    
    // MARK:- Variables
    var articles = [ArticleDataModel]()
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        articlesTV.registerCellNib(cellClass: ArticleTVCell.self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        articlesTV.isHidden = true
        loadArticles()
    }
    
    //MARK:- Load_Articles
    func loadArticles() {
        self.showLoadingIndicator()
        FirebaseAuthManager.shared.getArticles(){ (articles, error) in
            self.hideLoadingIndicator()
            if let err = error {
                self.showAlertError(title: "Error".localized, body: err.localizedDescription)
            }else{
                guard let articles = articles else{return}
                if articles.count > 0{
                    self.articlesTV.isHidden = false
                }
                self.articles = articles
                self.articlesTV.reloadData()
                DispatchQueue.main.async {
                    self.animateTV(tableView: self.articlesTV)
                }
            }
        }
    }
}
