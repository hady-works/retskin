//
//  AddAboutVC.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/14/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

class AddAboutVC: BaseViewController {
    
    //MARK:- Instance
    static func instance () -> AddAboutVC{
        let storyboard = UIStoryboard.init(name: "AdminSettings", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "AddAboutVC") as! AddAboutVC
    }
    
    //MARK:- Outlets
    @IBOutlet weak var versionView: UIView!
    @IBOutlet weak var versionTxtField: UITextField!
    @IBOutlet weak var copyrightView: UIView!
    @IBOutlet weak var copyrightTxtField: UITextField!
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var phoneTxtField: UITextField!
    @IBOutlet weak var descView: UIView!
    @IBOutlet weak var descTxtView: UITextView!
    @IBOutlet weak var saveBtn: UIButton!
    
    //MARK:- Instance Variables
    var edit = false
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
        setupUI()
    }
    
    //MARK:- SetupComponents
    func setupComponents() {
        versionView.addCornerRadius()
        copyrightView.addCornerRadius()
        descView.addCornerRadius(10)
        phoneView.addCornerRadius()
        saveBtn.addCornerRadius()
        descTxtView.textColor = .lightGray
        descTxtView.text = "type a description".localized
        descTxtView.delegate = self
    }
    
    func setupUI() {
        self.showLoadingIndicator()
        FirebaseAuthManager.shared.getAbout(completionBlock: { (about, error)  in
            self.hideLoadingIndicator()
            if let err = error {
                self.showAlertError(title: "Error".localized, body: err.localizedDescription)
            }else{
                guard let about = about else{return}
                self.versionTxtField.text = about.version
                self.copyrightTxtField.text = about.copyright
                self.phoneTxtField.text = about.phoneNum
                self.descTxtView.text = about.description
                self.descTxtView.textColor = .black
                self.saveBtn.setTitle("Edit".localized, for: .normal)
                self.edit = true
            }
        })
    }
    
    //MARK:- Actions
    @IBAction func buBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buSave(_ sender: Any) {
        if validData() {
            let dict = ["version": versionTxtField.text ?? "",
                        "copyright": copyrightTxtField.text ?? "",
                        "phoneNum": phoneTxtField.text ?? "",
                        "description": descTxtView.text ?? ""]
            
            self.showLoadingIndicator()
            FirebaseAuthManager.shared.addData(childName: "about", value: dict, completionBlock: { (status,error) in
                self.hideLoadingIndicator()
                if status{
                    self.showAlertsuccess(title: "Updated Successfully".localized)
                    self.dismiss(animated: true, completion: nil)
                }else{
                    self.showAlertError(title: "Error in sending data".localized)
                }
            })
        }
    }
    
    // MARK:- Data Validations
    func validData() -> Bool{
        
        if (versionTxtField.text!.isEmpty || copyrightTxtField.text!.isEmpty || phoneTxtField.text!.isEmpty || descTxtView.text!.isEmpty || descTxtView.text == "type a description" ){
            self.showAlertWiring(title: "All Fields are required".localized)
            return false
        }
        
        if copyrightTxtField.text!.count < 8{
            self.showAlertWiring(title: "Copyright should be at least 7 characters".localized)
            return false
        }
        
        if descTxtView.text!.count < 8{
            self.showAlertWiring(title: "Description should be at least 7 characters".localized)
            return false
        }
        
        return true
    }
    
}
