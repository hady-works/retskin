//
//  AddAboutVC+Extensions.swift
//  RetSkin
//
//  Created by Hady Hammad on 6/26/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

// MARK:- TextView Extension
extension AddAboutVC: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == .lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "type a description".localized
            textView.textColor = .lightGray
        }
    }
    
}
