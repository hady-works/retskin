//
//  ChangePasswordVC.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/6/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

class ChangePasswordVC: BaseViewController {
    
    //MARK:- Instance
    static func instance () -> ChangePasswordVC{
        let storyboard = UIStoryboard.init(name: "AdminSettings", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
    }
    
    //MARK:- Outlets
    @IBOutlet weak var currentPwdView: UIView!
    @IBOutlet weak var currentPwdTxtField: UITextField!
    @IBOutlet weak var newPwdView: UIView!
    @IBOutlet weak var newPwdTxtField: UITextField!
    @IBOutlet weak var confirmPwdView: UIView!
    @IBOutlet weak var confirmPwdTxtField: UITextField!
    @IBOutlet weak var saveBtn: UIButton!
    
    //MARK:- Instance Variables
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
    }
    
    //MARK:- SetupComponents
    func setupComponents() {
        currentPwdView.addCornerRadius(20)
        newPwdView.addCornerRadius(20)
        confirmPwdView.addCornerRadius(20)
        saveBtn.addCornerRadius(20)
    }
    
    //MARK:- Actions
    @IBAction func buBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func updatePasswordAction(_ sender: Any) {
        if validData() {
            self.showLoadingIndicator()
            FirebaseAuthManager.shared.updateUserPassword(newPassword: newPwdTxtField.text!) { [weak self] (success, error) in
                guard let self = self else { return }
                self.hideLoadingIndicator()
                if (success) {
                    self.showAlertsuccess(title: "Sucessfully change your password".localized)
                    self.dismiss(animated: true, completion: nil)
                } else {
                    self.showAlertError(title: "Failed to change your password".localized, body: error?.localizedDescription ?? "")
                }
            }
        }
    }
    
    // MARK:- Data Validations
    func validData() -> Bool{
        
        if (newPwdTxtField.text!.isEmpty || confirmPwdTxtField.text!.isEmpty){
            self.showAlertWiring(title: "All Fields are required".localized)
            return false
        }
        
        if currentPwdTxtField.text!.count <= 5{
            self.showAlertWiring(title: "Password must be greater than or equal 6".localized)
            return false
        }
        
        if newPwdTxtField.text!.count <= 5{
            self.showAlertWiring(title: "Password must be greater than or equal 6".localized)
            return false
        }
        
        if confirmPwdTxtField.text != newPwdTxtField.text{
            self.showAlertWiring(title: "Confirm password not match".localized)
            return false
        }
        
        return true
    }
    
}
