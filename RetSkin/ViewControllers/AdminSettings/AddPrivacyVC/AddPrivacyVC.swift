//
//  AddPrivacyVC.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/14/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

class AddPrivacyVC: BaseViewController {
    
    //MARK:- Instance
    static func instance () -> AddPrivacyVC{
        let storyboard = UIStoryboard.init(name: "AdminSettings", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "AddPrivacyVC") as! AddPrivacyVC
    }
    
    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var saveBtn: UIButton!
    
    //MARK:- Instance Variables
    var privacyArray = [PrivacyDataModel]()
    var privacyCount = 0
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
        loadPrivacy()
    }
    
    //MARK:- SetupComponents
    func setupComponents() {
        tableView.registerCellNib(cellClass: AdminPrivacyTVC.self)
        saveBtn.addCornerRadius(20)
    }
    
    //MARK:- Load Privacy
    func loadPrivacy() {
        self.showLoadingIndicator()
        FirebaseAuthManager.shared.getPrivacy(completionBlock: { (response, error)  in
            self.hideLoadingIndicator()
            if let err = error {
                self.showAlertError(title: "Error".localized, body: err.localizedDescription)
            }else{
                guard let privacy = response, privacy.count != 0 else{return}
                self.privacyCount = privacy.count
                self.privacyArray = privacy
                self.tableView.reloadData()
            }
        })
    }
    
    // MARK:- Collect TableViews Data
    func getPrivacy() ->[[String:Any]]?{
        var values = [[String:Any]]()
        for (index, _) in privacyArray.enumerated() {
            let indexPath = IndexPath(row: index, section: 0)
            guard let cell = tableView.cellForRow(at: indexPath) as? AdminPrivacyTVC else{
                return nil
            }
            if let text = cell.titleTxtField.text, !text.isEmpty, let descText = cell.descTxtView.text, !descText.isEmpty, descText != "type a description"{
                let dict = ["title": text,
                            "description": descText]
                values.append(dict)
            }
        }
        return values
    }
    
    //MARK:- Table Cells Actions
    func removePrivacy(index: IndexPath){
        
        if index.row < privacyCount {
            FirebaseAuthManager.shared.removePrivacy(privacy: self.privacyArray[index.row], completionBlock: { (msg, err) in
                if err == nil {
                    self.showAlertsuccess(title: msg)
                    self.privacyArray.remove(at: index.row)
                    self.tableView.reloadData()
                }else{
                    self.showAlertError(title: msg)
                }
            })
        }else{
            self.privacyArray.remove(at: index.row)
            self.tableView.reloadData()
        }
    }
    
    //MARK:- Actions
    @IBAction func buBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buSave(_ sender: Any) {
        guard let privacyData = getPrivacy(), privacyData.count == self.privacyArray.count else{
            self.showAlertWiring(title: "All Fields are required".localized)
            return
        }
        
        self.showLoadingIndicator()
        FirebaseAuthManager.shared.addPrivacyData(childName: "privacy", value: privacyData, completionBlock: { (status,error) in
            self.hideLoadingIndicator()
            if status{
                self.showAlertsuccess(title: "Updated Successfully".localized)
                self.loadPrivacy()
            }else{
                self.showAlertError(title: "Error in sending data".localized)
            }
        })
    }
    
    @IBAction func buAddMore(_ sender: Any) {
        let privacy = PrivacyDataModel(key: "null", privacy: [:])
        self.privacyArray.append(privacy)
        self.tableView.reloadData()
    }
    
}
