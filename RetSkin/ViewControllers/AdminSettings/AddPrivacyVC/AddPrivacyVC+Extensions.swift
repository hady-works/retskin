//
//  AddPrivacyVC+Extensions.swift
//  RetSkin
//
//  Created by Hady Hammad on 6/26/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

extension AddPrivacyVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return privacyArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue() as AdminPrivacyTVC
        let privacy = self.privacyArray[indexPath.row]
        cell.configure(privacy: privacy)
        cell.removeAction = { [weak self] in
            guard let self = self else {return}
            self.removePrivacy(index: indexPath)
        }
        return cell
    }
    
}
