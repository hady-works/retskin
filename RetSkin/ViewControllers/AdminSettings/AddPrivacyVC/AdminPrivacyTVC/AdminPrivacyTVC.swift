//
//  AdminPrivacyTVC.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/14/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

class AdminPrivacyTVC: UITableViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var titleTxtField: UITextField!
    @IBOutlet weak var descView: UIView!
    @IBOutlet weak var descTxtView: UITextView!
    @IBOutlet weak var removeBtn: UIButton!
    
    //MARK:- Properties
    var privacy: PrivacyDataModel?
    var removeAction: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupComponents()
    }
    
    //MARK:- SetupComponents
    func setupComponents() {
        titleView.addCornerRadius(12)
        descView.addCornerRadius(12)
        descTxtView.textColor = .lightGray
        descTxtView.text = "type a description".localized
        descTxtView.delegate = self
    }
    
    //MARK:- Configure
    func configure(privacy: PrivacyDataModel) {
        self.descTxtView.textColor = .black
        self.titleTxtField.text  = privacy.title
        if let desc = privacy.description {
            self.descTxtView.text = desc
        }else{
            self.descTxtView.textColor = .lightGray
            self.descTxtView.text = "type a description".localized
        }
    }
    
    //MARK:- Action's
    @IBAction func removeActionTapped(_ sender: Any) {
        removeAction?()
    }
    
}

// MARK:- TextView Extension
extension AdminPrivacyTVC: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == .lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "type a description".localized
            textView.textColor = .lightGray
        }
    }
    
}
