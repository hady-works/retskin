//
//  AdminHelpVC+Extensions.swift
//  RetSkin
//
//  Created by Hady Hammad on 6/26/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

// MARK:- TableView Extension
extension AdminHelpVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return helpsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue() as HelpTVC
        let help = self.helpsArray[indexPath.row]
        cell.configure(help: help)
        cell.mailAction = { [weak self] email in
            guard let self = self else {return}
            self.sendEmail(email: email)
        }
        return cell
    }
    
}
