//
//  AdminHelpVC.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/17/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit
import MessageUI

class AdminHelpVC: BaseViewController, MFMailComposeViewControllerDelegate {
    
    //MARK:- Instance
    static func instance () -> AdminHelpVC{
        let storyboard = UIStoryboard.init(name: "AdminSettings", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "AdminHelpVC") as! AdminHelpVC
    }
    
    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- Instance Variables
    var helpsArray = [HelpDataModel]()
    var mail: MFMailComposeViewController!
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerCellNib(cellClass: HelpTVC.self)
        loadHelps()
    }
    
    //MARK:- Load Privacy
    func loadHelps() {
        self.showLoadingIndicator()
        FirebaseAuthManager.shared.getHelps(completionBlock: { (response, error)  in
            self.hideLoadingIndicator()
            if let err = error {
                self.showAlertError(title: "Error".localized, body: err.localizedDescription)
            }else{
                guard let helps = response, helps.count != 0 else{return}
                self.helpsArray = helps
                self.tableView.reloadData()
            }
        })
    }
   
    func sendEmail(email: String) {
        let mailComposeViewController = configureMailComposer(email: email)
        if MFMailComposeViewController.canSendMail(){
            self.present(mailComposeViewController, animated: true, completion: nil)
        }else{
            print("Can't send email")
        }
    }
    
    func configureMailComposer(email: String) -> MFMailComposeViewController{
        let mailComposeVC = MFMailComposeViewController()
        mailComposeVC.mailComposeDelegate = self
        mailComposeVC.setToRecipients([email])
        mailComposeVC.setSubject("RetSkin")
        mailComposeVC.setMessageBody("", isHTML: false)
        return mailComposeVC
    }
    
    //MARK:- Actions
    @IBAction func buBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - MFMail compose method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}
