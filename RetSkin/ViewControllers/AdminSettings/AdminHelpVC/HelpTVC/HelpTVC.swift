//
//  HelpTVC.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/18/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

class HelpTVC: UITableViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var userEmailLbl: UILabel!
    @IBOutlet weak var messageTxtView: UITextView!
    
    //MARK:- Properties
    var help: HelpDataModel?
    var mailAction: ((_ email: String) -> ())?
    
    //MARK:- LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupComponents()
    }
    
    //MARK:- SetupComponents
    func setupComponents() {
        mainView.addCornerRadius(16)
        mainView.addBorderWith(width: 0.3, color: .lightGray)
        userEmailLbl.underline()
        userEmailLbl.addGestureRecognizer(UITapGestureRecognizer())
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(tappedOnLabel(_ :)))
        tapgesture.numberOfTapsRequired = 1
        self.userEmailLbl.addGestureRecognizer(tapgesture)
    }
    
    //MARK:- Configure
    func configure(help: HelpDataModel) {
        self.help = help
        self.userNameLbl.text = help.name
        self.userEmailLbl.text = help.email
        self.messageTxtView.text = help.message
    }
 
    //MARK:- tappedOnLabel
    @objc func tappedOnLabel(_ gesture: UITapGestureRecognizer) {
        if let email = self.help?.email {
            mailAction?(email)
        }
    }
    
}
