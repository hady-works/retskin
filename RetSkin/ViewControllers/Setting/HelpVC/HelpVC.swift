//
//  HelpVC.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/17/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit
import Firebase

class HelpVC: BaseViewController {
    
    //MARK:- Instance
    static func instance () -> HelpVC{
        let storyboard = UIStoryboard.init(name: "Settings", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "HelpVC") as! HelpVC
    }
    
    //MARK:- Outlets
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var nameTxtField: UITextField!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var messageTxtView: UITextView!
    @IBOutlet weak var saveBtn: UIButton!
    
    //MARK:- Instance Variables
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
    }
    
    //MARK:- SetupComponents
    func setupComponents() {
        nameView.addCornerRadius(10)
        emailView.addCornerRadius(10)
        messageView.addCornerRadius(10)
        saveBtn.addCornerRadius(10)
        messageTxtView.textColor = .lightGray
        messageTxtView.text = "type a message".localized
        messageTxtView.delegate = self
    }
    
    //MARK:- Actions
    @IBAction func buBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buSend(_ sender: Any) {
        
        if validData(){
            let dict = ["name": nameTxtField.text ?? "",
                        "email": emailTxtField.text ?? "",
                        "message": messageTxtView.text ?? ""]
            
            self.showLoadingIndicator()
            FirebaseAuthManager.shared.addDataWithChild(childName: "helps", value: dict, completionBlock: { (status,error) in
                self.hideLoadingIndicator()
                if status{
                    self.showAlertsuccess(title: "Message sent successfully".localized,body: "Wait until the support team contact with you".localized)
                    self.dismiss(animated: true, completion: nil)
                }else{
                    self.showAlertError(title: "Error in sending message".localized)
                }
            })
        }
    }
    
    // MARK:- Data Validations
    func validData() -> Bool{
        
        if (nameTxtField.text!.isEmpty || emailTxtField.text!.isEmpty || messageTxtView.text!.isEmpty){
            self.showAlertWiring(title: "All Fields are required".localized)
            return false
        }
        
        if nameTxtField.text!.count < 3{
            self.showAlertWiring(title: "Please enter valid name".localized)
            return false
        }
    
        if !(emailTxtField.text!.isValidEmail){
            self.showAlertWiring(title: "Enter valid email".localized)
            return false
        }
        
        if messageTxtView.text!.count < 3{
            self.showAlertWiring(title: "Please enter valid message".localized)
            return false
        }
        
        return true
    }
}
