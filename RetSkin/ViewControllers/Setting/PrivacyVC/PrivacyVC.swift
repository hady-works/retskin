//
//  PrivacyVC.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/6/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit
import MOLH

class PrivacyVC: UIViewController {
    
    //MARK:- Instance
    static func instance () -> PrivacyVC{
        let storyboard = UIStoryboard.init(name: "Settings", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "PrivacyVC") as! PrivacyVC
    }
    
    //MARK:- Outlets
    @IBOutlet weak var privacyTxtView: UITextView!
    
    //MARK:- Instance Variables
    let text = "Control your privacy You can read the privacy notice for more information.".localized
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        privacyTxtView.delegate = self
        setupAttributedText(textView: privacyTxtView)
        privacyTxtView.textAlignment = MOLHLanguage.isArabic() ? .right : .left
    }
    
    func setupAttributedText(textView: UITextView) {
        
        textView.linkTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.mainColor]
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(.foregroundColor, value: UIColor.labelColor, range: NSRange(location: 0, length: text.count - 1))
        
        attributedString.addAttribute(.font, value: UIFont(name: "Gilroy-Medium", size: 18)!, range: NSRange(location: 0, length: text.count - 1))
        
        if MOLHLanguage.isArabic() {
            attributedString.addAttribute(.link, value: "privacy", range: NSRange(location: 29, length: 16))
            attributedString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 29, length: 16))
        }else{
            attributedString.addAttribute(.link, value: "privacy", range: NSRange(location: 37, length: 15))
            attributedString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 37, length: 15))
        }
        
        textView.attributedText = attributedString
    }
    
    //MARK:- Actions
    @IBAction func buBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
