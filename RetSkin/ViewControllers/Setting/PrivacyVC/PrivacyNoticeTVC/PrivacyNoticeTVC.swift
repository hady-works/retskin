//
//  PrivacyNoticeTVC.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/8/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

class PrivacyNoticeTVC: UITableViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK:- Configure
    func configure(privacy: PrivacyDataModel) {
        self.titleLbl.text  = privacy.title
        self.descriptionLbl.text = privacy.description
    }
    
}
