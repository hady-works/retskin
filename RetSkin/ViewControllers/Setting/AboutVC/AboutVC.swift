//
//  AboutVC.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/6/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

class AboutVC: BaseViewController {
    
    //MARK:- Instance
    static func instance (about: AboutDataModel?) -> AboutVC{
        let vc = UIStoryboard.init(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "AboutVC") as! AboutVC
        vc.about = about
        return vc
    }
    
    //MARK:- Outlets
    @IBOutlet weak var versionLbl: UILabel!
    @IBOutlet weak var copyrightLbl: UILabel!
    @IBOutlet weak var descTxtView: UITextView!
    
    //MARK:- Variables
    var about: AboutDataModel?
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        guard let about = about else{return}
        self.versionLbl.text = about.version
        self.copyrightLbl.text = about.copyright
        self.descTxtView.text = about.description
    }
    
    //MARK:- Actions
    @IBAction func buBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
