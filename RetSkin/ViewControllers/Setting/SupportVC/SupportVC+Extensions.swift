//
//  SupportVC+Extensions.swift
//  RetSkin
//
//  Created by Hady Hammad on 6/26/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

extension SupportVC: UITextViewDelegate{
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        if URL.absoluteString == "contact" {
            print("Contact Support")
        }
        return false
    }
}
