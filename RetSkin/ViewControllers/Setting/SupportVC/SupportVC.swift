//
//  SupportVC.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/6/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit
import MOLH

class SupportVC: UIViewController {

    //MARK:- Instance
    static func instance () -> SupportVC{
        let storyboard = UIStoryboard.init(name: "Settings", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "SupportVC") as! SupportVC
    }
    
    //MARK:- Outlets
    @IBOutlet weak var contactUsTxtView: UITextView!
    
    //MARK:- Instance Variables
    let text = "If you have any inquiry or need help with products, please Contact Support for more information.".localized
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        contactUsTxtView.delegate = self
        setupAttributedText(textView: contactUsTxtView)
        contactUsTxtView.textAlignment = MOLHLanguage.isArabic() ? .right : .left
    }
    
    func setupAttributedText(textView: UITextView) {
        
        textView.linkTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.mainColor]
        let attributedString = NSMutableAttributedString(string: text)
        
        attributedString.addAttribute(.foregroundColor, value: UIColor.labelColor, range: NSRange(location: 0, length: text.count - 1))
        
        attributedString.addAttribute(.font, value: UIFont(name: "Gilroy-Medium", size: 18)!, range: NSRange(location: 0, length: text.count - 1))
        
        if MOLHLanguage.isArabic() {
            attributedString.addAttribute(.link, value: "contact", range: NSRange(location: 67, length: 15))
            attributedString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 67, length: 15))
        }else{
            attributedString.addAttribute(.link, value: "contact", range: NSRange(location: 59, length: 15))
            attributedString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 59, length: 15))
        }
        
        textView.attributedText = attributedString
    }
    
    //MARK:- Actions
    @IBAction func buBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
