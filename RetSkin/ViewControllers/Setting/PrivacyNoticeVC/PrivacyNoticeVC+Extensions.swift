//
//  PrivacyNoticeVC+Extensions.swift
//  RetSkin
//
//  Created by Hady Hammad on 6/26/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

//MARK:- TableView Delegate And Data Source
extension PrivacyNoticeVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return privacyArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue() as PrivacyNoticeTVC
        cell.configure(privacy: privacyArray[indexPath.row])
        return cell
    }
    
}
