//
//  PrivacyNoticeVC.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/6/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

class PrivacyNoticeVC: BaseViewController {
    
    //MARK:- Instance
    static func instance () -> PrivacyNoticeVC{
        let storyboard = UIStoryboard.init(name: "Settings", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "PrivacyNoticeVC") as! PrivacyNoticeVC
    }
    
    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- Instance Variables
    var privacyArray = [PrivacyDataModel]()
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerCellNib(cellClass: PrivacyNoticeTVC.self)
        loadPrivacy()
    }
    
    //MARK:- Load Privacy
    func loadPrivacy() {
        self.showLoadingIndicator()
        FirebaseAuthManager.shared.getPrivacy(completionBlock: { (response, error)  in
            self.hideLoadingIndicator()
            if let err = error {
                self.showAlertError(title: "Error".localized, body: err.localizedDescription)
            }else{
                guard let privacy = response else{return}
                self.privacyArray = privacy
                self.tableView.reloadData()
            }
        })
    }
    
    //MARK:- Actions
    @IBAction func buBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
