//
//  SettingsVC+Extensions.swift
//  RetSkin
//
//  Created by Hady Hammad on 6/28/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

extension SettingsVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settingsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = settingsTV.dequeue() as SettingTVCell
        cell.configure(setting: settingsArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
            
        case 0:
            let vc = AboutVC.instance(about: self.about)
            present(vc, animated: true, completion: nil)
            
        case 1:
            let vc = PrivacyNoticeVC.instance()
            present(vc, animated: true, completion: nil)
            
        case 2:
            let vc = HelpVC.instance()
            present(vc, animated: true, completion: nil)
            
        case 3:
            //open whatsapp
            guard let phone = about?.phoneNum else{return}
            print(phone)
            let whatsappURL = URL(string: "https://api.whatsapp.com/send?phone=\(phone)")
            if UIApplication.shared.canOpenURL(whatsappURL!) {
                UIApplication.shared.openURL(whatsappURL!)
            }
        case 4:
            let vc = LoginVC.instance()
            present(vc, animated: true, completion: nil)
            
        case 5:
            self.changeLanguage()
            
        default:
            print("error")
        }
        
    }
    
}

