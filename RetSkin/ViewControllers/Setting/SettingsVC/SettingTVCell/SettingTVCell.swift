//
//  SettingTVCell.swift
//  RetSkin
//
//  Created by Hady Hammad on 6/28/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

class SettingTVCell: UITableViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var settingTitleLbl: UILabel!
    @IBOutlet weak var iconImageVIew: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK:- Configure
    func configure(setting: String) {
        self.settingTitleLbl.text  = setting.localized
        self.iconImageVIew.image = setting == "Language" ? UIImage(named: "group5") : UIImage(named: setting)
    }
    
}
