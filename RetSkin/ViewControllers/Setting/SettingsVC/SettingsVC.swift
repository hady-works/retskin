//
//  SettingsVC.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/10/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit
import MOLH

class SettingsVC: UIViewController {
    
    //MARK:- Outlet's
    @IBOutlet weak var settingsTV: UITableView!
    
    // MARK:- Instance
    static func instance () -> SettingsVC{
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
    }
    
    // MARK:- Variables
    var settingsArray = ["About the app", "Privacy", "Help", "Support", "Admin", "Language"]
    var about: AboutDataModel?
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        settingsTV.registerCellNib(cellClass: SettingTVCell.self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        FirebaseAuthManager.shared.getAbout(completionBlock: { (about, error)  in
            self.about = about
        })
        DispatchQueue.main.async {
            self.animateTVList(tableView: self.settingsTV)
        }
    }
    
    // MARK: - Helper's
    func changeLanguage(){
        
        AlertUtility.showActionSheet(titleText: "Choose Language".localized, titleFont: UIFont(name: "Gilroy-Medium", size: 18)!, titleColor: .mainColor, cancelTitle: "Cancel".localized, otherTitles: ["English", "العربية"], VC: self, completion: { (index) in
            
            if index == 1 {
                if MOLHLanguage.currentAppleLanguage() == "ar"{
                    MOLHLanguage.setDefaultLanguage("en")
                    MOLH.setLanguageTo("en")
                    self.setCollectionAppearance()
                    Constants.isAdmin = false
                    MOLH.reset()
                }
            }
            if index == 2 {
                if MOLHLanguage.currentAppleLanguage() == "en"{
                    MOLHLanguage.setDefaultLanguage("ar")
                    MOLH.setLanguageTo("ar")
                    self.setCollectionAppearance()
                    Constants.isAdmin = false
                    MOLH.reset()
                }
                
            }
        })
        
    }
    
    func setCollectionAppearance() {
        UICollectionView.appearance().semanticContentAttribute = MOLHLanguage.isRTLLanguage() ? .forceRightToLeft : .forceLeftToRight
    }
    
}
