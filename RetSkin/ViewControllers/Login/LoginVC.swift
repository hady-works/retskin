//
//  LoginVC.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/5/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

class LoginVC: BaseViewController {
    
    //MARK:- Instance
    static func instance () -> LoginVC{
        let storyboard = UIStoryboard.init(name: "Login", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
    }
    
    //MARK:- Outlets
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var passwordTxtField: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    
    //MARK:- Instance Variables
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
    }
    
    //MARK:- SetupComponents
    func setupComponents() {
        emailView.addCornerRadius(20)
        passwordView.addCornerRadius(20)
        loginBtn.addCornerRadius(20)
    }
    
    //MARK:- Actions
    @IBAction func buLogin(_ sender: Any) {
        if validData() {
            self.showLoadingIndicator()
            FirebaseAuthManager.shared.signIn(email: emailTxtField.text!, pass: passwordTxtField.text!) {[weak self] (success, error) in
                guard let self = self else { return }
                self.hideLoadingIndicator()
                if (success) {
                    self.showAlertsuccess(title: "Login Success".localized)
                    Constants.isLogin = true
                    let mainVC = AdminTabBar.instance()
                    self.present(mainVC, animated: true, completion: nil)
                } else {
                    self.showAlertError(title: "Login Failed".localized, body: error?.localizedDescription ?? "")
                }
            }
        }
        
    }
    
    // MARK:- Data Validations
    func validData() -> Bool{
        if (emailTxtField.text!.isEmpty || passwordTxtField.text!.isEmpty){
            self.showAlertWiring(title: "All Fields are required".localized)
            return false
        }
        
        if !emailTxtField.text!.isValidEmail{
            showAlertWiring(title: "Enter valid email".localized)
            return false
        }
        
        if passwordTxtField.text!.count <= 5{
            self.showAlertWiring(title: "Wrong password".localized, body: "Password must be greater than or equal 6".localized)
            return false
        }
        
        return true
    }
    
    @IBAction func buForgotPassword(_ sender: Any) {
        present(ForgotPasswordVC.instance(), animated: true, completion: nil)
    }
    
    @IBAction func buBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
