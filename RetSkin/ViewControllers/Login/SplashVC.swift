//
//  SplashVC.swift
//  RetSkin
//
//  Created by Hady on 8/31/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit
import AVFoundation

class SplashVC: UIViewController {
    
    //MARK:- Instance
    static func instance () -> SplashVC{
        let storyboard = UIStoryboard.init(name: "Login", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "SplashVC") as! SplashVC
    }
    
    var player: AVPlayer?
    
    //MARK:- LifeCycle
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.finishedPlaying(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player)
        loadVideo()
    }
    
    //MARK:- Notification Function
    @objc func finishedPlaying( _ myNotification:NSNotification) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = Constants.getIsLogin() ? AdminTabBar.instance() : MainTabBar.instance()
    }
    
    private func loadVideo() {
        
        //this line is important to prevent background music stop
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.ambient)
        } catch { }
        
        let path = Bundle.main.path(forResource: "retskin intro 2", ofType:"mp4")
        
        player = AVPlayer(url: NSURL(fileURLWithPath: path!) as URL)
        let playerLayer = AVPlayerLayer(player: player)
        
        playerLayer.frame = self.view.frame
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerLayer.zPosition = -1
        
        self.view.layer.addSublayer(playerLayer)
        
        player?.seek(to: CMTime.zero)
        player?.play()
    }
    
}
