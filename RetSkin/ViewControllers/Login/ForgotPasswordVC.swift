//
//  ForgotPasswordVC.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/17/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

class ForgotPasswordVC: BaseViewController {
    
    //MARK:- Instance
    static func instance () -> ForgotPasswordVC{
        let storyboard = UIStoryboard.init(name: "Login", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
    }
    
    //MARK:- Outlets
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var sendBtn: UIButton!
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        emailView.addCornerRadius(8)
        sendBtn.addCornerRadius(8)
    }
    
    //MARK:- Actions
    @IBAction func buForgotPassword(_ sender: Any) {
        guard let email = emailTxtField.text else {return }
        
        if  email.isEmpty{
            showAlertWiring(title: "Please enter your email".localized)
            return
        }
        
        if  !email.isValidEmail{
            showAlertWiring(title: "Enter valid email".localized)
            return
        }
        
        self.showLoadingIndicator()
        FirebaseAuthManager.shared.resetPassword(email: email) {[weak self] (success, error) in
            guard let self = self else { return }
            self.hideLoadingIndicator()
            if (success) {
                self.showAlertsuccess(title: "Sucessfully Password Recovery".localized, body: "New Password is sent to your email".localized)
                self.dismiss(animated: true, completion: nil)
                
            } else {
                self.showAlertError(title: "Password Recovery Failed".localized, body: error?.localizedDescription ?? "")
            }
        }
    }
    
    @IBAction func buBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
