//
//  FirebaseAuthManager.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/5/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage

class FirebaseAuthManager {
    
    static var shared = FirebaseAuthManager()
    let userId = Auth.auth().currentUser?.uid
    
    func signIn(email: String, pass: String, completionBlock: @escaping (_ success: Bool, _ error: Error?) -> Void) {
        Auth.auth().signIn(withEmail: email, password: pass) { (result, error) in
            if let error = error, let _ = AuthErrorCode(rawValue: error._code) {
                completionBlock(false, error)
            } else {
                completionBlock(true, nil)
            }
        }
    }
    
    func resetPassword(email: String, completionBlock: @escaping (_ success: Bool, _ error: Error?) -> Void){
        
        Auth.auth().sendPasswordReset(withEmail: email ){ error in
            if let error = error {
                completionBlock(false, error)
            }else{
                completionBlock(true, nil)
            }
        }
    }
    
    func updateUserPassword(newPassword: String, completionBlock: @escaping (_ success: Bool, _ error: Error?) -> Void) {
        Auth.auth().currentUser?.updatePassword(to: newPassword, completion: { (error) in
            if let error = error {
                completionBlock(false, error)
            } else {
                completionBlock(true, nil)
            }
        })
    }
    
    func uploadImage(storageName: String, image: UIImage, completionBlock: @escaping (_ success: Bool, _ url: String?) -> Void) {
        guard let uploadData = image.jpegData(compressionQuality: 0.5) else { return }
        let filename = "\(NSUUID().uuidString).jpeg"
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        let storageRef = Storage.storage().reference().child(storageName).child(filename)
        storageRef.putData(uploadData, metadata: metadata) { (dataURl, error) in
            storageRef.downloadURL { (url, error) in
                if let url = url {
                    completionBlock(true, url.absoluteString)
                } else {
                    completionBlock( false, nil)
                }
            }
        }
    }
    
    func addProductData(value: [String:Any], completionBlock: @escaping (_ success: Bool, _ error: Error?) -> Void) {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM yyyy"
        formatter.timeZone = TimeZone.current
        let date = formatter.string(from: Date())
        
        if let day = formatter.date(from: date)?.timeIntervalSince1970 {
            let dayValue = Int(day * 1000)
            var proValue = value
            let ref = Database.database().reference().child("product").child("\(dayValue)").childByAutoId()
            proValue["id"] = ref.key
            
            ref.setValue(proValue) { (error, referrnce) in
                if let error = error {
                    completionBlock(false, error)
                }else{
                    completionBlock(true, nil)
                }
            }
        }
        
    }
    
    func addArticle(value: [String:Any], completionBlock: @escaping (_ success: Bool, _ error: Error?) -> Void) {
        
        var proValue = value
        let ref = Database.database().reference().child("articles").childByAutoId()
        proValue["id"] = ref.key
        
        ref.setValue(proValue) { (error, referrnce) in
            if let error = error {
                completionBlock(false, error)
            }else{
                completionBlock(true, nil)
            }
        }
        
    }
    
    func updateProductData(id: String, date: String, value: [String:Any], completionBlock: @escaping (_ success: Bool, _ error: Error?) -> Void) {
        let ref = Database.database().reference().child("product").child(date).child(id)
        ref.updateChildValues(value){  (error, referrnce) in
            if let error = error {
                completionBlock(false, error)
            }else{
                completionBlock(true, nil)
            }
        }
    }
    
    func addDataWithChild(childName: String, value: [String:Any], completionBlock: @escaping (_ success: Bool, _ error: Error?) -> Void) {
        let ref = Database.database().reference().child(childName).childByAutoId()
        ref.setValue(value) { (error, referrnce) in
            if let error = error {
                completionBlock(false, error)
            }else{
                completionBlock(true, nil)
            }
        }
    }
    
    func addData(childName: String, value: [String:Any], completionBlock: @escaping (_ success: Bool, _ error: Error?) -> Void) {
        let ref = Database.database().reference().child(childName)
        ref.setValue(value) { (error, referrnce) in
            if let error = error {
                completionBlock(false, error)
            }else{
                completionBlock(true, nil)
            }
        }
    }
    
    func getAbout(completionBlock: @escaping (_ about: AboutDataModel?, _ error: Error?) -> Void) {
        let ref = Database.database().reference().child("about")
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            if let dict = snapshot.value as? [String:Any]{
                let about = AboutDataModel(about: dict)
                completionBlock(about, nil)
            }
        }) { (err) in
            completionBlock(nil, err)
        }
    }
    
    func addPrivacyData(childName: String, value: [[String:Any]], completionBlock: @escaping (_ success: Bool, _ error: Error?) -> Void) {
        let ref = Database.database().reference().child(childName)
        ref.setValue(value) { (error, referrnce) in
            if let error = error {
                completionBlock(false, error)
            }else{
                completionBlock(true, nil)
            }
        }
    }
    
    func getPrivacy(completionBlock: @escaping (_ privacy: [PrivacyDataModel]?, _ error: Error?) -> Void) {
        var privacyArray = [PrivacyDataModel]()
        let ref = Database.database().reference().child("privacy")
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            if let snapshot = snapshot.children.allObjects as? [DataSnapshot]{
                for snap in snapshot{
                    let key = snap.key
                    if let dict = snap.value as? Dictionary<String,AnyObject>{
                        let type = PrivacyDataModel(key: key, privacy: dict)
                        privacyArray.append(type)
                    }
                }
            }
            completionBlock(privacyArray, nil)
        }) { (err) in
            completionBlock(nil, err)
        }
    }
    
    func getHelps(completionBlock: @escaping (_ help: [HelpDataModel]?, _ error: Error?) -> Void) {
        var helpsArray = [HelpDataModel]()
        let ref = Database.database().reference().child("helps")
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            if let snapshot = snapshot.children.allObjects as? [DataSnapshot]{
                for snap in snapshot{
                    if let dict = snap.value as? Dictionary<String,AnyObject>{
                        let type = HelpDataModel(help: dict)
                        helpsArray.append(type)
                    }
                }
            }
            completionBlock(helpsArray, nil)
        }) { (err) in
            completionBlock(nil, err)
        }
    }
    
    func getBodyParts(completionBlock: @escaping (_ posts: [BodyPartDataModel]?) -> Void) {
        var bodyPartArray = [BodyPartDataModel]()
        let ref = Database.database().reference().child("bodyPart")
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            if let snapshot = snapshot.children.allObjects as? [DataSnapshot]{
                for snap in snapshot{
                    if let dict = snap.value as? Dictionary<String,AnyObject>{
                        let type = BodyPartDataModel(bodyPart: dict)
                        bodyPartArray.append(type)
                    }
                }
            }
            completionBlock(bodyPartArray)
        })
    }
    
    func getSortedProducts(completionBlock: @escaping (_ products: [String:[ProductDataModel]]?, _ error: Error?) -> Void) {
        var productArray = [String:[ProductDataModel]]()
        var products = [ProductDataModel]()
        let ref = Database.database().reference().child("product")
        ref.observe(.value, with: { (snapshot) in
            if let snapshot = snapshot.children.allObjects as? [DataSnapshot]{
                for snap in snapshot{
                    
                    let key = snap.key
                    
                    if let snapshot = snap.children.allObjects as? [DataSnapshot]{
                        for snap in snapshot{
                            if let dict = snap.value as? Dictionary<String,Any>{
                                let type = ProductDataModel(key: key, product: dict)
                                products.append(type)
                            }
                        }
                        productArray[key] = products
                        products.removeAll()
                    }
                    
                }
            }
            completionBlock(productArray, nil)
        }) { (err) in
            completionBlock(nil, err)
        }
    }
    
    func getProducts(completionBlock: @escaping (_ products: [ProductDataModel]?, _ error: Error?)  -> Void) {
        var productArray = [ProductDataModel]()
        let ref = Database.database().reference().child("product")
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            if let snapshot = snapshot.children.allObjects as? [DataSnapshot]{
                for snap in snapshot{
                    let key = snap.key
                    if let snapshot = snap.children.allObjects as? [DataSnapshot]{
                        for snap in snapshot{
                            if let dict = snap.value as? Dictionary<String,AnyObject>{
                                let type = ProductDataModel(key: key, product: dict)
                                productArray.append(type)
                            }
                        }
                    }
                    
                }
            }
            completionBlock(productArray, nil)
        }) { (err) in
            completionBlock(nil, err)
        }
    }
    
    func getArticles(completionBlock: @escaping (_ articles: [ArticleDataModel]?, _ error: Error?) -> Void) {
        var articleArray = [ArticleDataModel]()
        let ref = Database.database().reference().child("articles")
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            if let snapshot = snapshot.children.allObjects as? [DataSnapshot]{
                for snap in snapshot{
                    if let dict = snap.value as? Dictionary<String,AnyObject>{
                        let article = ArticleDataModel(key: snap.key, product: dict)
                        articleArray.append(article)
                    }
                }
            }
            completionBlock(articleArray, nil)
        }) { (err) in
            completionBlock(nil, err)
        }
    }
    
    func getBodyPartById(id: Int, completionBlock: @escaping (_ bodyPart: BodyPartDataModel?) -> Void) {
        let ref = Database.database().reference().child("bodyPart").child("\(id)")
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            if let dict = snapshot.value as? [String:Any]{
                let part = BodyPartDataModel(bodyPart: dict)
                completionBlock(part)
            }
        })
    }
    
    func removeProduct(product: ProductDataModel, completionBlock: @escaping (_ successMsg: String, _ errorMsg: Error?) -> Void) {
        guard let productId = product.id, let date = product.date  else{return}
        let ref = Database.database().reference().child("product").child(date).child(productId)
        
        ref.removeValue { (error, referrnce) in
            if let error = error {
                completionBlock("Failed to remove product".localized, error)
            }else{
                completionBlock("Successfully remove product".localized, nil)
            }
        }
    }
    
    func removeArticle(withKey key: String, completionBlock: @escaping (_ successMsg: String, _ errorMsg: Error?) -> Void) {
        let ref = Database.database().reference().child("articles").child(key)
        
        ref.removeValue { (error, referrnce) in
            if let error = error {
                completionBlock("Failed to remove article".localized, error)
            }else{
                completionBlock("Successfully remove article".localized, nil)
            }
        }
    }
    
    func removePrivacy(privacy: PrivacyDataModel, completionBlock: @escaping (_ successMsg: String, _ errorMsg: Error?) -> Void) {
        guard let id = privacy.id else{return}
        let ref = Database.database().reference().child("privacy").child(id)
        
        ref.removeValue { (error, referrnce) in
            if let error = error {
                completionBlock("Failed to remove privacy", error)
            }else{
                completionBlock("Successfully remove privacy", nil)
            }
        }
    }
    
    func logout(completionBlock: @escaping (_ success: Bool, _ error: Error?) -> Void) {
        
        do {
            try Auth.auth().signOut()
            completionBlock(true, nil)
            
        } catch let err {
            
            completionBlock(false, err)
        }
    }
    
}
