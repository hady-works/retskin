//
//  Constants.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/5/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

class Constants{
    
    static var isAdmin = false
    //MARK:- Devices
    static var devices: [String] = []{
        didSet{
            UserDefaults.standard.set(devices, forKey: "devices")
        }
    }
    
    static func getDevices() -> [String] {
        if let devices = UserDefaults.standard.value(forKey: "devices") as? [String]{
            Constants.devices = devices
        }
        return Constants.devices
    }
    
    static func logoutDevicesCleanUp() {
        Constants.devices = []
        UserDefaults.standard.removeObject(forKey: "devices")
    }
    
    //MARK:- Reminders
    static var reminders: [String] = []{
        didSet{
            UserDefaults.standard.set(reminders, forKey: "reminders")
        }
    }
    
    static func getReminders() -> [String] {
        if let reminders = UserDefaults.standard.value(forKey: "reminders") as? [String]{
            Constants.reminders = reminders
        }
        return Constants.reminders
    }
    
    //MARK:- IsLogin
    static var isLogin: Bool = false{
        didSet{
            UserDefaults.standard.set(isLogin, forKey: "isLogin")
        }
    }
    
    static func getIsLogin() -> Bool {
        if let isLogin = UserDefaults.standard.value(forKey: "isLogin") as? Bool{
            Constants.isLogin = isLogin
        }
        return Constants.isLogin
    }
}
