//
//  ArticleDataModel.swift
//  RetSkin
//
//  Created by Hady on 9/5/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import Foundation

struct ArticleDataModel {
    
    var adminId : String?
    var id : String?
    var articleTitleAr : String?
    var articleTitleEn : String?
    var bodyPart : Int?
    var partLogo : String?
    var articleContentAr : String?
    var articleContentEn : String?
    var image : String?
    var createdAt: Date?
    
    
    init(key: String, product: Dictionary<String,Any>) {
        self.adminId = product["adminId"] as? String
        self.id = product["id"] as? String
        self.articleTitleAr = product["articleTitleAr"] as? String
        self.articleTitleEn = product["articleTitleEn"] as? String
        self.bodyPart = product["part"] as? Int
        self.partLogo = product["partLogo"] as? String
        self.articleContentEn = product["articleContentEn"] as? String
        self.articleContentAr = product["articleContentAr"] as? String
        self.image = product["articleImage"] as? String
        if let timestamp = product["creationDate"] as? Double{
            self.createdAt = Date(timeIntervalSince1970: timestamp)
        }
    }
}
