//
//  ProductDataModel.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/5/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import Foundation

struct ProductDataModel {
    
    var adminId : String?
    var id : String?
    var bodyPart : Int?
    var arDescription : String?
    var enDescription : String?
    var partLogo : String?
    var arName : String?
    var enName : String?
    var arSpecification : [String]?
    var enSpecification : [String]?
    var images : [String]?
    var repetitionList: [String]?
    var createdAt: Date?
    var date: String?
    var startDate: Date?
    var eventIDs: [String]?
    var repetition: String?
    
    init(key: String, product: Dictionary<String,Any>) {
        self.adminId = product["adminId"] as? String
        self.id = product["id"] as? String
        self.bodyPart = product["part"] as? Int
        self.arDescription = product["productDescAr"] as? String
        self.enDescription = product["productDescEn"] as? String
        self.partLogo = product["partLogo"] as? String
        self.arName = product["productTitleAr"] as? String
        self.enName = product["productTitleEn"] as? String
        self.arSpecification = product["productSpecAr"] as? [String]
        self.enSpecification = product["productSpecEn"] as? [String]
        self.images = product["productImg"] as? [String]
        self.repetitionList = product["repetitionsList"] as? [String]
        if let timestamp = product["creationDate"] as? Double{
            self.createdAt = Date(timeIntervalSince1970: timestamp)
        }
        
        self.date = key
    }
    
    init(device: Device) {
        self.id = device.id
        self.bodyPart = Int(device.deviceBodyPart)
        self.arDescription = device.deviceArDesc
        self.enDescription = device.deviceEnDesc
        self.partLogo = device.partLogo
        self.arName = device.deviceArName
        self.enName = device.deviceEnName
        self.arSpecification = device.deviceArSpec as? [String]
        self.enSpecification = device.deviceEnSpec as? [String]
        self.images = device.deviceImages as? [String]
        self.repetitionList = device.repetitions as? [String]
    }
    
    init(reminder: Reminder) {
        self.id = reminder.id
        self.bodyPart = Int(reminder.deviceBodyPart)
        self.arDescription = reminder.deviceArDesc
        self.enDescription = reminder.deviceEnDesc
        self.partLogo = reminder.partLogo
        self.arName = reminder.deviceArName
        self.enName = reminder.deviceEnName
        self.arSpecification = reminder.deviceArSpec as? [String]
        self.enSpecification = reminder.deviceEnSpec as? [String]
        self.images = reminder.deviceImages as? [String]
        self.repetitionList = reminder.repetitionsList as? [String]
        self.createdAt = reminder.time
        self.startDate = reminder.startDate
        self.eventIDs = reminder.eventsIdentifiers as? [String]
        self.repetition = reminder.repetition as? String
    }
    
}
