//
//  PrivacyDataModel.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/14/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import Foundation

struct PrivacyDataModel {
    
    var id : String?
    var title : String?
    var description : String?
    
    init(key: String, privacy: Dictionary<String,Any>) {
        self.id = key
        self.title = privacy["title"] as? String
        self.description = privacy["description"] as? String
    }
}
