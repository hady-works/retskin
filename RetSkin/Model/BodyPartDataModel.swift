//
//  BodyPartDataModel.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/8/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import Foundation

struct BodyPartDataModel {
    
    var id : Int?
    var bodyName : String?
    var imageURL : String?
    var logo : String?
    
    init(bodyPart: Dictionary<String,Any>) {
        self.id = bodyPart["id"] as? Int
        self.bodyName = bodyPart["bodyTitle"] as? String
        self.imageURL = bodyPart["imageUrl"] as? String
        self.logo = bodyPart["logo"] as? String
    }
}
