//
//  DataModel.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/18/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import Foundation
struct HelpDataModel {
    
    var name : String?
    var email : String?
    var message : String?
    
    init(help: Dictionary<String,Any>) {
        self.name = help["name"] as? String
        self.email = help["email"] as? String
        self.message = help["message"] as? String
    }
}
