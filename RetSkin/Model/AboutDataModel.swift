//
//  AboutDataModel.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/14/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import Foundation

struct AboutDataModel {
    
    var version : String?
    var copyright : String?
    var phoneNum : String?
    var description : String?
    
    init(about: Dictionary<String,Any>) {
        self.version = about["version"] as? String
        self.copyright = about["copyright"] as? String
        self.phoneNum = about["phoneNum"] as? String
        self.description = about["description"] as? String
    }
}
