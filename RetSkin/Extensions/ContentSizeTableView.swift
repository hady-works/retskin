//
//  ContentSizeTableView.swift
//  RetSkin
//
//  Created by Hady Hammad on 6/1/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import Foundation
import UIKit

class ContentSizeTableView: UITableView {
    override var contentSize: CGSize{
        didSet{
            invalidateIntrinsicContentSize()
        }
    }
    
    override var intrinsicContentSize: CGSize{
        layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height)
    }
}
