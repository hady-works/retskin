//
//  UIColor+Extensions.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/5/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit

extension UIColor{
    static var backColor = UIColor(red: 233/255, green: 239/255, blue: 241/255, alpha: 1)
    static var mainColor = UIColor(red: 197/255, green: 74/255, blue: 141/255, alpha: 1)
    static var labelColor = UIColor(red: 46/255, green: 46/255, blue: 46/255, alpha: 1)
    static var weekBorderColor = UIColor(red: 163/255, green: 169/255, blue: 174/255, alpha: 1)
}
