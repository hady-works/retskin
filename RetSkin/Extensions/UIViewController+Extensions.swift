//
//  UIViewController+Extensions.swift
//  RetSkin
//
//  Created by Hady on 8/18/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit
import ViewAnimator

extension UIViewController {
    
    @objc func onDismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc func onDismiss() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func animateTVList(tableView: UITableView) {
        let animations = [AnimationType.from(direction: .bottom, offset: 30.0)]
        UIView.animate(views: tableView.visibleCells, animations: animations, completion: {
        })
    }
    
    func animateTV(tableView: UITableView) {
        let rotateAnimation = AnimationType.rotate(angle: CGFloat.pi/6)
        let zoomAnimation = AnimationType.zoom(scale: 0.2)
        UIView.animate(views: tableView.visibleCells,
                       animations: [rotateAnimation, zoomAnimation], delay: 0.65)
    }
    
    func animateTVHome(tableView: UITableView) {
        let fromAnimation = AnimationType.from(direction: .right, offset: 30.0)
        let zoomAnimation = AnimationType.zoom(scale: 0.2)
        UIView.animate(views: tableView.visibleCells,
                       animations: [fromAnimation, zoomAnimation], delay: 0.2)
    }
    
    func animateCVList(collectionView: UICollectionView) {
        let zoomAnimation = AnimationType.zoom(scale: 0.2)
        let rotateAnimation = AnimationType.rotate(angle: CGFloat.pi/6)
        UIView.animate(views: collectionView.visibleCells,
                       animations: [zoomAnimation, rotateAnimation],
                       duration: 0.7)
    }
    
    func animateTVAsCVList(tableView: UITableView) {
        let zoomAnimation = AnimationType.zoom(scale: 0.2)
        let rotateAnimation = AnimationType.rotate(angle: CGFloat.pi/6)
        UIView.animate(views: tableView.visibleCells,
                       animations: [zoomAnimation, rotateAnimation],
                       duration: 0.5)
    }
}

