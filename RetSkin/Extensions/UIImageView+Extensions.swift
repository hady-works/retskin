//
//  UIImageView+Extensions.swift
//  RetSkin
//
//  Created by Hady Hammad on 5/5/20.
//  Copyright © 2020 RetSkin. All rights reserved.
//

import UIKit
import SDWebImage
let NoImageName = "user image"

extension UIImageView{
    func setImage(imageUrl: String){
        self.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage.init(named: NoImageName), options: .continueInBackground) { (imagee, _,_ , _) in
            if let img = imagee{
                self.image = img
            }else{
                self.image = UIImage.init(named: NoImageName)
            }
        }
    }
}
